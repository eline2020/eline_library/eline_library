<?php

use Eline\Client\TcpClient;
use Eline\DbConnection\Db;

if (!function_exists('tcpClient')) {
    /**
     * 获取客户端
     * @param string $serverName
     * @return TcpClient
     * @throws Exception
     */
    function tcpClient(string $serverName): TcpClient
    {
        $configInfo = [
            'server_host'   => 'microservice.com',
            'port'          => '20219',
            'timeout'       => 60
        ];
        return TcpClient::getService($serverName, $configInfo);
    }
}

if (!function_exists('serviceRpc')) {
    /**
     * 获取服务Rpc
     * @param string $serverName
     * @param string $className
     * @param array $constructParams
     * @return TcpClient
     * @throws Exception
     */
    function serviceRpc(string $serverName, string $className, array $constructParams = []): TcpClient
    {
        return tcpClient($serverName)->handler($className, $constructParams);
    }
}

if (!function_exists('getAdminDb')) {
    /**
     * 获取数据库连接
     * @param string $dbName
     * @param bool $isMaster
     * @return mixed
     * @throws mixed
     */
    function getAdminDb(string $dbName, bool $isMaster = false)
    {
        $connect = Db::connection($dbName);
        return $isMaster ? $connect->master() : $connect;
    }
}