<?php

namespace Eline\WorkWechat\Message;

use Eline\Exception\BusinessException;
use Hyperf\Amqp\Message\ProducerMessage;
use Hyperf\Context\Context;

class BaseMessage extends ProducerMessage
{
    /**
     * 构造函数
     * @return void
     * @throws BusinessException
     */
    public function __construct()
    {
        if (is_array($this->payload)) {
            $handler = Context::get('handler');
            $method = Context::get('method');
            $this->payload['route'] = $handler . '===>' . $method;
        } else {
            throw new BusinessException('payload not array.', [], 1020);
        }
    }
}