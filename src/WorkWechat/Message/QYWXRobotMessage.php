<?php

namespace Eline\WorkWechat\Message;

use Eline\Exception\BusinessException;
use Hyperf\Amqp\Annotation\Producer;
use Hyperf\Amqp\Message\Type;

/**
 * @Producer()
 */
class QYWXRobotMessage extends BaseMessage
{
    /**
     * 交换机Key
     * @var string
     */
    protected $exchange = 'robot_message_exchange';

    /**
     * 数据类型
     * @var string
     */
    protected $type = Type::DIRECT;

    /**
     * 机器人消息
     * @param $robotKey
     * @param array $msgData
     * @param string $msgType
     * @throws BusinessException
     */
    public function __construct($robotKey, array $msgData, string $msgType = 'text')
    {
        $this->routingKey = 'robot_message_queue';

        $this->payload = [];
        $this->payload['robotKey'] = $robotKey;
        $this->payload['msgtype'] = $msgType;
        $this->payload[$msgType] = $msgData;
        parent::__construct();
    }
}