<?php

namespace Eline\WorkWechat;

use Eline\Exception\ApiException;
use Eline\WorkWechat\Common\Utils;
use Eline\WorkWechat\Robot\MessageInterface;

class RobotMessage
{
    /**
     * 机器人类型映射方法
     * @return array
     */
    const MSG_TYPE = [
        'text' => 'TextMessageContent',
        'image' => 'ImageMessageContent',
        'markdown' => 'MarkdownMessageContent',
        'news' => 'NewsMessageContent',
        'file' => 'FileMessageContent'
    ];

    /**
     * 机器人消息接口
     * @var ?MessageInterface
     */
    public ?MessageInterface $messageContent = null;

    /**
     * 机器人key
     * @var string|null
     */
    public ?string $robotKey = null;

    /**
     * 检查消息发送参数
     * @return void
     * @throws ApiException
     */
    public function checkMessageSendArgs()
    {
        if (is_null($this->messageContent)) {
            throw new ApiException("messageContent is empty");
        }
        Utils::checkNotEmptyStr($this->robotKey, 'robotKey');
        $this->messageContent->checkMessageSendArgs();
    }

    /**
     * 消息内容转数组
     * @return array
     */
    public function messageTOArray(): array
    {
        $args = array();
        $this->messageContent->messageContentToArray($args);
        return $args;
    }
}