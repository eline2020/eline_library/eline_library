<?php

namespace Eline\WorkWechat\Common;

use Eline\Exception\ApiException;

class Utils
{
    /**
     * 判断是不是Null
     * @param $var
     * @param $name
     * @param $args
     */
    static public function setIfNotNull($var, $name, &$args)
    {
        if (!is_null($var)) {
            $args[$name] = $var;
        }
    }

    /**
     * 检查是否是空字符串
     * @param $value
     * @param $name
     * @throws ApiException
     */
    public static function checkNotEmptyStr($value, $name)
    {
        if (!self::notEmptyStr($value)) {
            throw new ApiException($name . ' can not be empty string');
        }
    }

    /**
     * 判断是否是空字符串
     * @param $value
     * @return bool
     */
    private static function notEmptyStr($value): bool
    {
        return is_string($value) && ($value != '');
    }
}