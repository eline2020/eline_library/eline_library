<?php

namespace Eline\WorkWechat;

use CURLFile;
use Exception;

class CorpApi
{
    /**
     * 请求接口来源
     * @var string
     */
    const SOURCE_FROM = 'work_wechat';

    /**
     * 请求头地址
     * @var string|array
     */
    const HEADERS = [];

    /**
     * 机器人请求地址
     * @var string
     */
    const WEB_HOOK_SEND = '/cgi-bin/webhook/send?key=ACCESS_KEY';

    /**
     * 机器人上传文件
     */
    const WEB_HOOK_UPLOAD_MEDIA = '/cgi-bin/webhook/upload_media?key=ACCESS_KEY&type=file';

    /**
     * 机器人发送消息
     * @param RobotMessage $message
     * @param null $key
     * @return bool
     * @throws Exception
     */
    public function robotSend(RobotMessage $message, $key = null): bool
    {
        $message->checkMessageSendArgs();
        $param = $message->messageTOArray();
        $config = config('work_wechat.default');
        $url = $config['base_url'] . self::WEB_HOOK_SEND;
        $realUrl = str_replace('ACCESS_KEY', $key, $url);
        $data[] = self::formatParams($realUrl, $param, 'post', 60);
        $result = guzHttpRequest($data);
        return $this->checkRequestReturn($result[self::SOURCE_FROM]);
    }

    /**
     * 机器人上传文件
     * @param string $robotKey
     * @param string $filePath
     * @return array
     */
    public function uploadFile(string $robotKey, string $filePath): array
    {
        // 构建请求Url
        $config = config('params.work_wechat');
        $url = $config['base_url'] . self::WEB_HOOK_UPLOAD_MEDIA;
        $realUrl = str_replace('ACCESS_KEY', $robotKey, $url);
        $result = ApiHelper::updateFile($realUrl, $filePath);
        $result = json_decode($result, true);
        if ($this->checkRequestReturn($result)) {
            return $result;
        }
        return [];
    }

    /**
     * 请求参数格式化
     * @param string $url
     * @param array $data
     * @param string $method
     * @param int $timeout
     * @param string $sourceNo
     * @return array
     */
    private function formatParams(string $url = '', array $data = [], string $method = 'post', int $timeout = 20, string $sourceNo = ''): array
    {
        $params = [];
        if ($method == 'get') {
            $params = ['query' => $data, 'headers' => self::HEADERS, 'url' => $url, 'method' => $method, 'source_from' => self::SOURCE_FROM,
                'timeout' => empty($timeout) ? 20 : $timeout, 'source_no' => empty($sourceNo) ? '' : $sourceNo];
        } elseif ($method == 'post') {
            $params = ['json' => $data, 'headers' => self::HEADERS, 'url' => $url, 'method' => $method, 'source_from' => self::SOURCE_FROM,
                'timeout' => empty($timeout) ? 20 : $timeout, 'source_no' => empty($sourceNo) ? '' : $sourceNo];
        } elseif ($method == 'file') {
            $params = [];
        }
        return $params;
    }

    /**
     * 检查请求返回值
     * @param array $result
     * @return bool
     */
    private function checkRequestReturn(array $result): bool
    {
        if (isset($result['errcode']) && $result['errcode'] == 0) {
            return true;
        }
        return false;
    }
}