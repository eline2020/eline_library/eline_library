<?php

namespace Eline\WorkWechat\Robot;

use Eline\WorkWechat\Common\Utils;
use Exception;

class TextMessageContent implements MessageInterface
{
    /**
     * 消息类型
     * @var string
     */
    public string $msg_type = 'text';

    /**
     * 消息内容
     * @var string|null
     */
    public ?string $content = null;

    /**
     * 是否全部@
     * @var bool
     */
    public bool $sendToAll = false;

    /**
     * @的企业微信ID
     * @var array|null
     */
    public ?array $mentioned_list = null;

    /**
     * @的手机号码
     * @var array|null
     */
    public ?array $mentioned_mobile_list = null;

    /**
     * 是否发送@所有人
     * @return void
     */
    public function sendToAll()
    {
        $this->sendToAll = true;
    }

    /**
     * 检查消息发送参数
     * @return void
     * @throws Exception
     */
    public function checkMessageSendArgs()
    {
        $len = strlen($this->content);
        if ($len == 0 || $len > 2048) {
            throw new Exception('invalid content length');
        }
    }

    /**
     * 消息内容转数组
     * @param $arr
     * @return void
     */
    public function messageContentToArray(&$arr)
    {
        Utils::setIfNotNull($this->msg_type, 'msg_type', $arr);
        $contentArr = array('content' => $this->content);
        if (true == $this->sendToAll) {
            Utils::setIfNotNull(['@all'], 'mentioned_list', $contentArr);
        } else {
            Utils::setIfNotNull($this->mentioned_list, 'mentioned_list', $contentArr);
            Utils::setIfNotNull($this->mentioned_mobile_list, 'mentioned_mobile_list', $contentArr);
        }
        Utils::setIfNotNull($contentArr, $this->msg_type, $arr);
    }
}