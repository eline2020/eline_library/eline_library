<?php

namespace Eline\WorkWechat\Robot;

use Eline\WorkWechat\Common\Utils;

class MarkdownMessageContent
{
    /**
     * 消息类型
     * @var string
     */
    public string $msg_type = 'markdown';

    /**
     * 消息内容
     * @var string|null
     */
    public ?string $content = null;

    /**
     * 是否全部@
     * @var bool
     */
    public bool $sendToAll = false;

    /**
     * 是否发送@所有人
     * @return void
     */
    public function sendToAll()
    {
        $this->sendToAll = true;
    }

    /**
     * 检查消息发送参数
     * @return void
     * @throws mixed
     */
    public function checkMessageSendArgs()
    {
        $len = strlen($this->content);
        if ($len == 0 || $len > 2048) {
            throw new \Exception('invalid content length');
        }
    }

    /**
     * 消息内容转数组
     * @param $arr
     * @return void
     */
    public function messageContentToArray(&$arr)
    {
        Utils::setIfNotNull($this->msg_type, 'msg_type', $arr);
        $contentArr = array('content' => $this->content);
        Utils::setIfNotNull($contentArr, $this->msg_type, $arr);
    }
}