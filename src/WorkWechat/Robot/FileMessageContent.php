<?php

namespace Eline\WorkWechat\Robot;

use Eline\WorkWechat\Common\Utils;

class FileMessageContent implements MessageInterface
{
    /**
     * 消息类型
     * @var string
     */
    public string $msg_type = 'file';

    /**
     * 文件媒体ID
     * @var string|null
     */
    public ?string $media_id = null;

    /**
     * 是否全部@
     * @var bool
     */
    public bool $sendToAll = false;

    /**
     * 是否发送@所有人
     * @return void
     */
    public function sendToAll()
    {
        $this->sendToAll = true;
    }

    /**
     * 检查消息发送参数
     * @return void
     * @throws mixed
     */
    public function checkMessageSendArgs()
    {
        Utils::checkNotEmptyStr($this->media_id, 'media_id');
    }

    /**
     * 消息内容转数组
     * @param $arr
     * @return void
     */
    public function messageContentToArray(&$arr)
    {
        Utils::setIfNotNull($this->msg_type, 'msg_type', $arr);
        $contentArr = array();
        Utils::setIfNotNull($this->media_id, 'media_id', $contentArr);
        Utils::setIfNotNull($contentArr, $this->msg_type, $arr);
    }
}