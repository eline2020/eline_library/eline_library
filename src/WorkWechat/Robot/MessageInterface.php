<?php

namespace Eline\WorkWechat\Robot;

interface MessageInterface
{
    /**
     * 是否发送@所有人
     * @return mixed
     */
    public function sendToAll();

    /**
     * 检查消息发送参数
     * @return mixed
     */
    public function checkMessageSendArgs();

    /**
     * 消息内容转数组
     * @param $arr
     * @return mixed
     */
    public function messageContentToArray(&$arr);
}