<?php

namespace Eline\WorkWechat\Robot;

use Eline\WorkWechat\Common\Utils;

class ImageMessageContent implements MessageInterface
{
    /**
     * 消息类型
     * @var string
     */
    public string $msg_type = 'image';

    /**
     * 图片base64
     * @var string|null
     */
    public ?string $base64 = null;

    /**
     * 文件md5
     * @var string|null
     */
    public ?string $md5 = null;

    /**
     * 是否全部@
     * @var bool
     */
    public bool $sendToAll = false;

    /**
     * @的企业微信ID
     * @var array|null
     */
    public ?array $mentioned_list = null;

    /**
     * @的手机号码
     * @var array|null
     */
    public ?array $mentioned_mobile_list = null;

    /**
     * 是否发送@所有人
     * @return void
     */
    public function sendToAll()
    {
        $this->sendToAll = true;
    }

    /**
     * 检查消息发送参数
     * @return void
     * @throws mixed
     */
    public function checkMessageSendArgs()
    {
        Utils::checkNotEmptyStr($this->base64, 'base64');
        Utils::checkNotEmptyStr($this->md5, 'md5');
    }

    /**
     * 消息内容转数组
     * @param $arr
     * @return void
     */
    public function messageContentToArray(&$arr)
    {
        Utils::setIfNotNull($this->msg_type, 'msg_type', $arr);
        $contentArr = array();
        Utils::setIfNotNull($this->base64, 'base64', $contentArr);
        Utils::setIfNotNull($this->md5, 'md5', $contentArr);
        if (true == $this->sendToAll) {
            Utils::setIfNotNull(['@all'], 'mentioned_list', $contentArr);
        } else {
            Utils::setIfNotNull($this->mentioned_list, 'mentioned_list', $contentArr);
            Utils::setIfNotNull($this->mentioned_mobile_list, 'mentioned_mobile_list', $contentArr);
        }
        Utils::setIfNotNull($contentArr, $this->msg_type, $arr);
    }
}