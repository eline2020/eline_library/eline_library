<?php

namespace Eline\WorkWechat\Robot;

use Eline\Exception\ApiException;
use Eline\WorkWechat\Common\Utils;

class NewsMessageContent implements MessageInterface
{
    /**
     * 消息类型
     * @var string
     */
    public string $msg_type = 'news';

    /**
     * 文件媒体ID
     * @var array|null
     */
    public ?array $articles = null;

    /**
     * @var array
     */
    public array $subClass = ['news' => 'NewsArticle'];

    /**
     * 是否全部@
     * @var bool
     */
    public bool $sendToAll = false;

    /**
     * 是否发送@所有人
     * @return void
     */
    public function sendToAll()
    {
        $this->sendToAll = true;
    }

    /**
     * 检查消息发送参数
     * @return void
     * @throws ApiException
     */
    public function checkMessageSendArgs()
    {
        $size = count($this->articles);
        if ($size < 1 || $size > 8) {
            throw new ApiException('1-8 articles should be given');
        }
        foreach ($this->articles as $item) {
            $item->checkMessageSendArgs();
        }
    }

    /**
     * 消息内容转数组
     * @param $arr
     * @return void
     */
    public function messageContentToArray(&$arr)
    {
        Utils::setIfNotNull($this->msg_type, 'msg_type', $arr);
        $articleList = array();
        foreach ($this->articles as $item) {
            $articleList[] = $item->articleToArray();
        }
        $arr[$this->msg_type]['articles'] = $articleList;
    }
}