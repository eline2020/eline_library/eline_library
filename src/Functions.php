<?php

use Eline\Services\ApiHelper;
use Eline\Tool\Json;

if (!function_exists('guzHttpRequest')) {
    /**
     * @param array $params
     * @return array
     * @throws Exception
     */
    function guzHttpRequest(array $params): array
    {
        return ApiHelper::guzHttpRequestWithLog($params);
    }
}

if (!function_exists('trace')) {
    /**
     * 记录日志
     * @param array|string $message
     * @throws mixed
     */
    function trace($message)
    {
        $message = is_array($message) || is_object($message) ? Json::encode($message) : $message;
        $logPath = RUNTIME_PATH . '/' . date('Y-m-d') . '.log';
        file_put_contents($logPath, $message . PHP_EOL, FILE_APPEND);
    }
}

if (!function_exists('getFullException')) {
    /**
     * @desc 错误格式化
     * @param Exception $exception
     * @param bool $withTrace
     * @return array
     */
    function getFullException(Exception $exception, bool $withTrace = false): array
    {
        return [
            'code' => $exception->getCode(),
            'message' => $exception->getMessage(),
            'file' => $exception->getFile(),
            'line' => $exception->getLine(),
            'trace' => $withTrace ? $exception->getTraceAsString() : ''
        ];
    }
}
