<?php

namespace Eline\Dao;

use think\Collection;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;
use think\db\Query;
use think\Model;

/**
 * Dao的基类
 */
abstract class Dao
{
    /**
     * 数据库实例列表
     * @var array 
     */
    private static array $_instances = [];

    /**
     * 获取表名称
     * @return string
     */
    abstract public function getTableName(): string;

    /**
     * 获取数据库对象实例
     * @return mixed | Query
     */
    abstract protected function db();

    /**
     * 构造函数
     * @return void 
     */
    public function __construct()
    {
    }

    /**
     * @param $method
     * @param $arguments
     * @return mixed
     */
    public static function __callStatic($method, $arguments)
    {
        return call_user_func([static::getInstance(), $method], $arguments);
    }

    /**
     * 获取单例
     * @return $this
     */
    public static function getInstance(): Dao
    {
        $class = get_called_class();
        if (!isset(self::$_instances[$class])) {
            self::$_instances[$class] = new $class();
        }
        return self::$_instances[$class];
    }

    /**
     * 根据条件返回单条数据信息
     * @param string|array $conditions
     * @param string|array $fields
     * @return array|mixed|Query|Model|null
     * @throws DataNotFoundException
     * @throws ModelNotFoundException
     * @throws DbException
     */
    public function getInfo($conditions, $fields = '*')
    {
        return $this->db()->table($this->getTableName())->field($fields)->where($conditions)->find();
    }

    /**
     * 根据条件返回多条信息
     * @param string|array $conditions
     * @param string|array $fields
     * @param int $page
     * @param int $pageSize
     * @param array $orderBy
     * @param array $groupBy
     * @return array|Query[]
     * @throws DataNotFoundException
     * @throws ModelNotFoundException
     * @throws DbException
     */
    public function getList($conditions, $fields = '*', int $page = 0, int $pageSize = 0, array $orderBy = [], array $groupBy = []): array
    {
        $selector = $this->db()->table($this->getTableName())->field($fields)->where($conditions);
        !empty($groupBy) && $selector->group($groupBy);
        !empty($orderBy) && $selector->order($orderBy);
        if (!empty($page)) {
            $pageSize = !empty($pageSize) ? $pageSize : 100;
            $selector->page($page, $pageSize);
        }
        return $selector->select()->toArray();
    }

    /**
     * 获取多条信息
     * @param $condition
     * @param $field
     * @param string $key
     * @param int $page
     * @param int $pageSize
     * @return array
     */
    public function column($condition, $field, string $key, int $page = 0, int $pageSize = 0): array
    {
        $selector = $this->db()->table($this->getTableName())->where($condition);
        if (!empty($page)) {
            $pageSize = !empty($pageSize) ? $pageSize : 100;
            $selector->page($page, $pageSize);
        }
        return $selector->column($field, $key);
    }

    /**
     * 插入一条数据
     * @param array $data
     * @param bool $strict
     * @return int|string
     */
    public function insert(array $data, bool $strict = true)
    {
        return $this->db()->table($this->getTableName())->strict($strict)->insert($data);
    }

    /**
     * 插入一条记录并返回主键ID
     * @param array $data
     * @param bool $strict
     * @return int|string
     */
    public function insertGetId(array $data, bool $strict = true)
    {
        return $this->db()->table($this->getTableName())->strict($strict)->insertGetId($data);
    }

    /**
     * 插入多条数据
     * @param array $data
     * @param bool $strict
     * @return int
     */
    public function insertAll(array $data, bool $strict = true): int
    {
        return $this->db()->table($this->getTableName())->strict($strict)->insertAll($data);
    }

    /**
     * 根据条件更新数据
     * @param string|array $condition
     * @param array $data
     * @return int
     * @throws DbException
     */
    public function update($condition, array $data): int
    {
        return $this->db()->table($this->getTableName())->where($condition)->update($data);
    }

    /**
     * 根据条件删除
     * @param string|array $condition
     * @return int
     * @throws DbException
     */
    public function delete($condition): int
    {
        return $this->db()->table($this->getTableName())->where($condition)->delete();
    }

    /**
     * 返回总条数
     * @param string|array $conditions
     * @return float|int
     * @throws DbException
     */
    public function count($conditions)
    {
        return $this->db()->table($this->getTableName())->where($conditions)->count();
    }

    /**
     * 返回字段汇总
     * @param string|array $conditions
     * @param string|array $field
     * @return float
     */
    public function sum($conditions, $field): float
    {
        return $this->db()->table($this->getTableName())->where($conditions)->sum($field);
    }

    /**
     * 返回字段最大值
     * @param $condition
     * @param string|array $field
     * @param bool $force
     * @return float|int|string
     */
    public function max($condition, $field, bool $force)
    {
        return $this->db()->table($this->getTableName())->where($condition)->max($field, $force);
    }

    /**
     * 返回字段最小值
     * @param $condition
     * @param string|array $field
     * @param bool $force
     * @return float|int|string
     */
    public function min($condition, $field, bool $force)
    {
        return $this->db()->table($this->getTableName())->where($condition)->min($field, $force);
    }

    /**
     * 返回字段平均值
     * @param $condition
     * @param string|array $field
     * @return float
     */
    public function avg($condition, $field): float
    {
        return $this->db()->table($this->getTableName())->where($condition)->avg($field);
    }

    /**
     * 获取最后执行的SQL 
     * @return string
     */
    public function getSql(): string
    {
        return $this->db()->getLastSql();
    }

    /**
     * 开启事务
     * @return void
     */
    public function startTrans()
    {
        $this->db()->startTrans();
    }

    /**
     * 提交事务 
     * @return void
     */
    public function commit()
    {
        $this->db()->commit();
    }

    /**
     * 回归事务
     * @return void
     */
    public function rollback()
    {
        $this->db()->rollback();
    }
}
