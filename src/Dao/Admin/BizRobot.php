<?php

namespace Eline\Dao\Admin;

use Eline\Dao\Dao;
use Eline\Dao\DbAdmin;

class BizRobot extends Dao
{
    use DbAdmin;
    /**
     * 表名称
     */
    const TABLE = 'biz_robot';

    /**
     * 获取表名
     * @return string
     */
    public function getTableName(): string
    {
        return self::TABLE;
    }
}