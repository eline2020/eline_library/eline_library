<?php

namespace Eline\Dao\Admin;

use Eline\Dao\Dao;
use Eline\Dao\DbAdmin;

class BizDataDownloadTaskDao extends Dao
{
    use DbAdmin;
    /**
     * 表名称
     */
    const TABLE = 'biz_data_download_task';

    /**
     * 获取表名
     * @return string
     */
    public function getTableName(): string
    {
        return self::TABLE;
    }
}