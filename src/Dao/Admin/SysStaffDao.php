<?php

namespace Eline\Dao\Admin;

use Eline\Dao\Dao;
use Eline\Dao\DbAdmin;

class SysStaffDao extends Dao
{
    use DbAdmin;
    /**
     * 表名称
     */
    const TABLE = 'sys_staff';

    /**
     * 获取表名
     * @return string
     */
    public function getTableName(): string
    {
        return self::TABLE;
    }
}
