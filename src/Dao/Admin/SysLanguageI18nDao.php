<?php

namespace Eline\Dao\Admin;

use Eline\Dao\Dao;
use Eline\Dao\DbAdmin;

class SysLanguageI18nDao extends Dao
{
    use DbAdmin;

    /**
     * 表名称
     */
    const TABLE = 'sys_language_i18n';

    /**
     * 获取表名称
     * @return string
     */
    public function getTableName(): string
    {
        return self::TABLE;
    }

    /**
     * @param $condition
     * @param array|string $fields
     * @param int $page
     * @param int $pageSize
     * @param array $groupBy
     * @param array $orderBy
     * @return array
     */
    public function getLanguageList($condition, $fields = ['*'], int $page = 0, int $pageSize = 0, array $groupBy = [], array $orderBy = []): array
    {
        $db = $this->db()->table($this->getTableName())->alias('i')
            ->leftJoin(SysLanguageSystemDao::getInstance()->getTableName() . ' s','i.i18n_key = s.i18n_key')
            ->where($condition);
        // 添加分组
        !empty($groupBy) && $db->group($groupBy);
        // 添加排序
        !empty($orderBy) && $db->order($orderBy);
        // 添加分页
        $page > 0 && $db->page($page, $pageSize);
        return $db->field($fields)->select()->toArray();
    }
}