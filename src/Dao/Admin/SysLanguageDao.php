<?php

namespace Eline\Dao\Admin;

use Eline\Dao\Dao;
use Eline\Dao\DbAdmin;

class SysLanguageDao extends Dao
{
    use DbAdmin;
    /**
     * 表名称
     */
    const TABLE = 'sys_language';

    /**
     * 获取表名称
     * @return string
     */
    public function getTableName(): string
    {
        return self::TABLE;
    }
}