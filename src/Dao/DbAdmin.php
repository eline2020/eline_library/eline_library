<?php

namespace Eline\Dao;

trait DbAdmin
{
    /**
     * 数据库明
     * @var string
     */
    protected string $dbName;

    /**
     * 设置数据库
     * @param string $dbName
     * @return $this
     */
    public function setDbName(string $dbName): DbAdmin
    {
        $this->dbName = $dbName;

        return $this;
    }

    /**
     * 设置数据库
     * @return string
     */
    protected function getDbName(): string
    {
        return $this->dbName ?? 'default';
    }

    /**
     * 返回wook库连接实例
     * @param bool $isMaster
     * @return mixed
     * @throws mixed
     */
    protected function db(bool $isMaster = false)
    {
        return getAdminDb($this->getDbName(), $isMaster);
    }
}
