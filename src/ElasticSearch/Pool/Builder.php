<?php

declare(strict_types=1);

namespace Eline\ElasticSearch\Pool;

use Hyperf\Logger\LoggerFactory;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Collection;

class Builder
{
    /**
     * 匹配查询会评分
     * @var string
     */
    const MUST = 'must';

    /**
     * 匹配查询取反
     * @var string
     */
    const MUST_NOT = 'must_not';

    /**
     *
     * @var string
     */
    const SHOULD = 'should';

    /**
     *
     * @var string
     */
    const SHOULD_NOT = 'should_not';

    /**
     * 匹配查询不会评分
     * @var string
     */
    const FILTER = 'filter';

    /**
     * 查询
     * @var string
     */
    const TYPE_QUERY = 'query';

    /**
     * 聚合
     * @var string
     */
    const TYPE_AGGS = 'aggs';

    /**
     * ElasticSearch客户端
     * @var \Elasticsearch\Client
     */
    protected \Elasticsearch\Client $client;

    /**
     * 基础查询
     * @var array
     */
    protected array $query;

    /**
     * 高亮显示
     * @var array|object
     */
    protected $highlight = [];

    /**
     * 查询SQL
     * @var array
     */
    protected array $sql;

    /**
     * 排序
     * @var array|object
     */
    protected $sort = [];

    /**
     * 查询模型
     * @var Model
     */
    protected Model $model;

    /**
     * 偏移量
     * @var int
     */
    protected int $take = 0;

    /**
     * 字段
     * @var array|object
     */
    private $fields;

    /**
     * 排除的字段
     * @var array
     */
    private array $excludes;

    /**
     * 操作类型
     * @var array
     */
    protected array $operate = ['=', '>', '<', '>=', '<=', '!=', '<>', 'in', 'not in', 'like', 'regex', 'prefix','match_phrase', 'between', 'exists', 'missing', 'likeLeft', 'likeRight'];

    /**
     * 分页
     * @param int $page
     * @param int $limit
     * @return Paginator
     */
    public function page(int $page = 1, int $limit = 100): Paginator
    {
        $offset = (($page - 1) * $limit) ?: 0;
        // 构建SQL语句
        $this->sql = $this->sqlCombine($offset, $limit);
        // 执行查询
        $result = $this->run('search', $this->sql);
        // 处理查询结果
        $collection = $this->getCollection($result['hits']['hits'] ?? []);
        return make(Paginator::class, ['items' => $collection, 'perPage' => $limit, 'currentPage' => $page]);
    }

    /**
     * 获取列表
     * @param int $limit
     * @return Collection
     */
    public function get(int $limit = 100): Collection
    {
        $limit = $this->take ?: $limit;
        // 构建SQL语句
        $this->sql = $this->sqlCombine(0, $limit);
        // 执行查询
        $result = $this->run('search', $this->sql);
        // 处理查询结果
        return $this->getCollection($result['hits']['hits'] ?? []);
    }

    /**
     * 查询第一条
     * @return Model
     */
    public function first(): Model
    {
        return $this->take(1)->get()->first();
    }

    /**
     * 设置偏移量
     * @param int $take
     * @return $this
     */
    public function take(int $take): Builder
    {
        $this->take = $take;
        return $this;
    }

    /**
     * 查找单条
     * @param $id
     * @return Model
     */
    public function find($id): Model
    {
        $this->sql = [
            'index' => $this->model->getIndex(),
            'id' => $id
        ];
        $result = $this->run('get', $this->sql);

        $this->model->setAttributes($result['_source'] ?? []);
        $this->model->setOriginal($result);
        return $this->model;
    }

    /**
     * 批量插入
     * @param array $values
     * @return Collection
     */
    public function insertAll(array $values): Collection
    {
        $body = [];
        foreach ($values as $value) {
            $body['body'][] = [
                'index' => [
                    '_index' => $this->model->getIndex(),
                ]
            ];
            $body['body'][] = $value;
        }
        $this->sql = $body;
        $result = $this->run('bulk', $this->sql);
        return collect($result['items'])->map(function ($value, $key) use ($values) {
            $model = $this->model->newInstance();
            $model->setAttributes(Arr::merge($values[$key] ?? [], ['_id' => $values['index']['_id'] ?? '']));
            $model->setOriginal($value);
            return $this->model;
        });
    }

    /**
     * 插入记录
     * @param array $value
     * @return Model|array
     */
    public function insert(array $value)
    {
        $body = Arr::except($value, ['id', 'routing', 'timestamp']);
        $except = Arr::only($value, ['id', 'routing', 'timestamp']);
        $this->sql = Arr::merge($except, [
            'index' => $this->model->getIndex(),
            'body' => $body
        ]);
        $result = $this->run('index', $this->sql);
        if (!empty($data['result']) && $data['result'] == 'created') {
            $this->model->setOriginal($result);
            $this->model->setAttributes(Arr::merge($body, ['_id' => $result['_id'] ?? '']));
            return $this->model->getAttributes();
        }
        return $result;
    }

    /**
     * update
     *
     * @param array $value
     * @param string $id
     * @return array
     */
    public function update(array $value, string $id): array
    {
        $result = $this->run('update', [[
            'index' => $this->model->getIndex(),
            'id' => $id,
            'body' => [
                'doc' => $value,
            ],
        ]]);
        if (!empty($result['result']) && ($result['result'] == 'updated' || $result['result'] == 'noop')) {
            $this->model->setOriginal($result);
            $this->model->setAttributes(['_id' => $result['_id'] ?? '']);
            return $this->model->getAttributes();
        }
        return $result;
    }

    /**
     * 删除记录
     * @param string $id
     * @return bool
     */
    public function delete(string $id): bool
    {
        $result = $this->run('delete', [[
            'index' => $this->model->getIndex(),
            'id' => $id,
        ]]);
        if (!empty($result['result']) && $result['result'] == 'deleted') {
            return true;
        }
        return false;
    }

    /**
     * 更新Mapping
     * @param array $mappings
     * @return mixed|null
     */
    public function updateMapping(array $mappings)
    {
        $mappings = collect($mappings)->map(function ($value, $key) {
            $valued = [];
            if (is_string($value)) {
                $valued['type'] = $value;
            }
            if (is_array($value)) {
                $valued = $value;
            }
            return $valued;
        })->toArray();
        $this->sql = [
            'index' => $this->model->getIndex(),
            'body' => [
                'properties' => $mappings
            ]
        ];
        return $this->run('indices.putMapping', $this->sql);
    }

    /**
     * 更新设置
     * @param array $settings
     * @return mixed|null
     */
    public function updateSetting(array $settings)
    {
        $this->sql = [
            'index' => $this->model->getIndex(),
            'body' => [
                'settings' => $settings
            ]
        ];
        return $this->run('putSettings', $this->sql);
    }

    /**
     * 创建索引
     * @param array $mappings
     * @param array $settings
     * @return mixed|null
     */
    public function createIndex(array $mappings = [], array $settings = [])
    {
        $mappings = Arr::merge(
            Collection::make($this->model->getCasts())->map(function ($value, $key) {
                $valued = [];
                if (is_string($value)) {
                    $valued['type'] = $value;
                }
                if (is_array($value)) {
                    $valued = $value;
                }
                return $valued;
            })->toArray(),
            Collection::make($mappings)->map(function ($value, $key) {
                $valued = [];
                if (is_string($value)) {
                    $valued['type'] = $value;
                }
                if (is_array($value)) {
                    $valued = $value;
                }
                return $valued;
            })->toArray()
        );
        $this->sql = [
            'index' => $this->model->getIndex(),
            'id' => '1',
            'body' => compact('mappings') + compact('settings')
        ];
        return $this->run('create', $this->sql);
    }

    /**
     *
     * @param string $field
     * @param $value
     * @return $this
     */
    public function matchPhrase(string $field, $value): Builder
    {
        $this->query['match_phrase'][$field] = $value;
        return $this;
    }

    /**
     *
     * @param string $field
     * @param $value
     * @return $this
     */
    public function match(string $field, $value): Builder
    {
        $this->query['match'][$field] = $value;
        return $this;
    }

    /**
     * 原生语法查询
     * @param array $dsl
     * @param int $limit
     * @return Collection
     */
    public function rawSearch(array $dsl, int $limit = 100): Collection
    {
        return $this->raw($dsl, self::TYPE_QUERY, $limit);
    }

    /**
     * 原生语法聚合
     * @param array $dsl
     * @return Collection
     */
    public function rawAggs(array $dsl): Collection
    {
        return $this->raw($dsl, self::TYPE_AGGS, 0);
    }

    /**
     * 执行原生SQL语法
     * @param array $dsl
     * @param string $type
     * @param int $limit
     * @return Collection
     */
    private function raw(array $dsl, string $type = self::TYPE_QUERY, int $limit = 100): Collection
    {
        $limit = $this->take ?: $limit;
        $this->query = $dsl;
        $this->sql = $this->sqlCombine(0, $limit, $type);
        $result = $this->run('search', $this->sql);
        $original = $result['hits']['hits'] ?? [];

        if ($type == self::TYPE_AGGS) {
            $original = $result['aggregations'] ?? [];
        }

        return Collection::make($original)->map(function ($value) use ($type) {
            $attributes = $value['_source'] ?? [];
            if (!empty($attributes)) {
                $attributes['id'] = $value['_id'] ?? '';
            }
            if ($type == self::TYPE_AGGS) {
                $attributes = $value['buckets'] ?? [];
            }
            $model = $this->model->newInstance();
            $model->setAttributes($attributes);
            $model->setOriginal($value);
            return $model;
        });
    }



    /**
     * 运行
     * @param $method
     * @param ...$parameters
     * @return mixed|null
     * @throws mixed
     */
    protected function run($method, ...$parameters)
    {
        $client = $this->client;
        $sql = $this->sql;
        if (strpos($method, '.')) {
            $methods = explode('.', $method);
            $method = $methods[1];
            $client = $client->{$methods[0]}();
        }
        ApplicationContext::getContainer()
            ->get(LoggerFactory::class)
            ->get('elasticsearch', 'default')
            ->debug('Elasticsearch run', compact('method', 'parameters', 'sql'));
        return call([$client, $method], $parameters);
    }

    public function highlight($fields)
    {
        $fields = (array)$fields;

        $fields = Collection::make($fields)
            ->map(function ($item) {
                return [
                    $item => new \stdClass()
                ];
            })->toArray();
        $this->highlight = compact('fields');
        return $this;
    }



    /**
     * Set a model instance for the model being queried.
     *
     * @param Model $model
     * @return $this
     */
    public function setModel(Model $model): Builder
    {
        $this->model = $model;
        $this->client = $model->getClient();
        $this->highlight = new \stdClass();
        $this->sort = new \stdClass();
        return $this;
    }

    /**
     * 构建SQL语句
     * @param int $offset
     * @param int $limit
     * @param string $type
     * @return array
     */
    private function sqlCombine(int $offset, int $limit, string $type = self::TYPE_QUERY): array
    {
        // 构建查询语句
        $this->sql = [
            'from' => $offset,
            'size' => $limit,
            'index' => $this->model->getIndex(),
            'body' => [
                'query' => $this->query,
                'highlight' => $this->highlight,
                'sort' => $this->sort,
            ]
        ];

        if (isset($this->query['post_filter'])) {
            // 将post_filter放到顶层
            $this->sql['body']['post_filter'] = $this->query['post_filter'];
            // 去掉query中之前拼接的post_filter
            unset($this->sql['body']['query']['post_filter']);
        }

        if (!empty($this->excludes)) {
            // 排除哪些字段,其他字段显示
            $this->sql['body']['_source']['excludes'] = $this->excludes;
        } else {
            $this->sql['body']['_source'] = $this->fields ?: new \stdClass();
        }

        // 聚合查询
        if ($type == self::TYPE_AGGS) {
            // 聚合,只显示聚合的结果
            $this->sql['size'] = 0;
            $this->sql['body']['aggs'] = $this->query['aggs'];
            // 去掉aggs
            unset($this->sql['body']['query']['aggs']);
            if (empty($this->sql['body']['query'])) {
                unset($this->sql['body']['query']);
            }
        }

        // 查询所有
        if (empty($this->query)) {
            $this->sql = [
                'from' => $offset,
                'size' => $limit,
                'index' => $this->model->getIndex(),
                'body' => [
                    'query' => [
                        'match_all' => new \stdClass()
                    ]
                ]
            ];
        }

        return $this->sql;
    }

    /**
     * 处理查询结果
     * @param $original
     * @return Collection
     */
    private function getCollection($original): Collection
    {
        return Collection::make($original)->map(function ($value) {
            $attributes = $value['_source'] ?? [];
            if (!empty($attributes)) {
                $attributes['id'] = $value['_id'] ?? 0;
            }
            $model = $this->model->newInstance();
            $model->setAttributes($attributes);
            $model->setOriginal($value);
            return $model;
        });
    }


    /**
     * 对于指定的查询，过滤返回的结果的某个索引字段必须有值
     * @param string $field
     * @return $this
     */
    public function exists(string $field): Builder
    {
        $this->parseQuery($field, 'exists', 'all', self::MUST);
        return $this;
    }

    /**
     *
     * @param string $field
     * @return $this
     */
    public function existsFilter(string $field): Builder
    {
        $this->parseQuery($field, 'exists', 'all');
        return $this;
    }

    /**
     *
     * @param string $field
     * @return $this
     */
    public function missing(string $field): Builder
    {
        $this->parseQuery($field, 'missing', 'all');
        return $this;
    }

    /**
     * 查询字段，可以是数组，也可以是字段
     * @param mixed ...$params - ['field1','field2', 'field3']/'field1,field2,field3'
     * @return $this
     */
    public function select(...$params): Builder
    {
        $fields = func_get_args();
        if (isset($fields[0]) && is_array($fields[0]) && !empty($fields[0])) {
            $fields = $fields[0];
        }
        $this->fields = $fields;
        return $this;
    }

    /**
     * 获取字段
     * @return array
     */
    public function getField(): array
    {
        return $this->fields;
    }

    /**
     * 排除不需要显示的字段，其余字段都查询出来
     * @param mixed ...$params
     * @return $this
     */
    public function excludes(...$params): Builder
    {
        $fields = func_get_args();
        if (isset($fields[0]) && is_array($fields[0]) && !empty($fields[0])) {
            $fields = $fields[0];
        }
        $this->excludes = $fields;
        return $this;
    }






















    /**
     * 排序
     * @param string $field
     * @param bool $sort
     * @return Builder
     */
    public function orderBy(string $field, bool $sort = false): Builder
    {
        $sort = strtolower((string)$sort);
        if (!in_array($this->sort, ['asc', 'desc'])) {
            $sort = 'asc';
        }
        $this->sort[] = [$field => ['order' => $sort]];

        return $this;
    }

    /**
     * 聚合操作(aggs)
     * @param string $field - 需要聚合的字段
     * @param string $sort - 排序 升序 asc or 降序 desc
     * @param string $sortField - 排序字段,默认使用内置排序(_count)
     * @return $this
     */
    public function groupBy(string $field, string $sort = 'asc', string $sortField = ''): Builder
    {
        $sort = strtolower($sort);
        if (!in_array($sort, ['asc', 'desc'])) {
            $sort = 'asc';
        }
        $aggsColumn = sprintf('aggs_%s', $field);
        $sortField = $sortField ?: '_count'; //默认内置排序
        $this->query[self::TYPE_AGGS][$aggsColumn] = [
            'terms' => [
                'field' => $field,
                'order' => [
                    $sortField => $sort
                ]
            ]
        ];
        return $this;
    }

    /**
     * 全局桶聚合
     * @return $this
     */
    public function groupGlobalBy(): Builder
    {
        $aggsAll = sprintf('aggs_%s', 'all');
        $this->query[self::TYPE_AGGS][$aggsAll] = [
            'global' => new \stdClass(),//全局桶没有参数
        ];
        return $this;
    }

    /**
     * 后置过滤器
     * 只有当你需要对搜索结果和聚合使用不同的过滤方式时才考虑使用post_filter。有时一些用户会直接在常规搜索中使用post_filter。
     * 不要这样做！post_filter会在查询之后才会被执行，因此会失去过滤在性能上帮助(比如缓存)。
     * post_filter应该只和聚合一起使用，并且仅当你使用了不同的过滤条件时。
     * @param string $field
     * @param string $operate
     * @param null $value
     * @return $this
     */
    public function postAggsFilter(string $field, string $operate = '', $value = null): Builder
    {
        $this->parseQuery($field, $operate, $value, self::FILTER, true);
        return $this;
    }

    /**
     * 柱状图统计
     * @param string $field
     * @param $step - 步长
     * @return $this
     */
    public function histogram(string $field, $step): Builder
    {
        $aggsColumn = sprintf('aggs_%s', $field);
        $this->query[self::TYPE_AGGS][$aggsColumn] = [
            'histogram' => [
                'field' => $field,
                'interval' => $step
            ]
        ];
        return $this;
    }

    /**
     * 时间图统计
     * @param string $field 聚合的字段
     * @param string $interval 步长, day month quarter year
     * @param string $format 日期格式
     * @param string $startDate 开始日期
     * @param string $endDate 结束日期
     * @param int $minDocCount 最小匹配数
     * @return $this
     */
    public function dateHistogram(string $field, string $interval = 'month', string $format = 'yyyy-MM-dd', string $startDate = '', string $endDate = '', int $minDocCount = 0): Builder
    {
        $aggsColumn = sprintf('aggs_%s', $field);
        $this->query[self::TYPE_AGGS][$aggsColumn] = [
            'date_histogram' => [
                'field' => $field,
                'interval' => $interval,
                'format' => $format,
                'min_doc_count' => $minDocCount,
            ]
        ];
        if (!empty($startDate)) {
            $this->query[self::TYPE_AGGS][$aggsColumn]['date_histogram']['extended_bounds']['min'] = $startDate;
        }
        if (!empty($endDate)) {
            $this->query[self::TYPE_AGGS][$aggsColumn]['date_histogram']['extended_bounds']['max'] = $startDate;
        }
        return $this;
    }

    /**
     * must评分查询,可以分词,当字段类型为keyword时不能分词,字段类型为text时可以分词
     * @param $field - 可传闭包或字段
     * @param string $operate
     * @param null $value
     * @return $this
     */
    public function where($field, string $operate = '', $value = null): Builder
    {
        $this->whereParse($field, $operate, $value, self::MUST);
        return $this;
    }

    /**
     * filter不评分查询,性能优于where,但不能分词查询
     * @param $field - 可传闭包或字段
     * @param string $operate
     * @param null $value
     * @return $this
     */
    public function whereFilter($field, string $operate = '', $value = null): Builder
    {
        $this->whereParse($field, $operate, $value, self::FILTER);
        return $this;
    }

    /**
     * must_not查询
     * @param $field - 可传闭包或字段
     * @param string $operate
     * @param null $value
     * @return $this
     */
    public function whereNot($field, string $operate = '', $value = null): Builder
    {
        $this->whereParse($field, $operate, $value, self::MUST_NOT);
        return $this;
    }

    /**
     *
     * @param string $field
     * @param $value
     * @return $this
     */
    public function whereIn(string $field, $value): Builder
    {
        return $this->where($field, 'in', $value);
    }

    /**
     *
     * @param string $field
     * @param $value
     * @return $this
     */
    public function whereInFilter(string $field, $value): Builder
    {
        return $this->whereFilter($field, 'in', $value);
    }

    /**
     *
     * @param string $field
     * @param $value
     * @return $this
     */
    public function whereNotIn(string $field, $value): Builder
    {
        return $this->where($field, 'not in', $value);
    }

    /**
     *
     * @param string $field
     * @param $value
     * @return $this
     */
    public function whereNotInFilter(string $field, $value): Builder
    {
        return $this->whereFilter($field, 'not in', $value);
    }

    /**
     * 模糊搜索，类似于sql中like '%字符%'，只作用于keyword 类型
     * @param string $field
     * @param $value
     * @return Builder
     */
    public function like(string $field, $value): Builder
    {
        return $this->where($field, 'like', $value);
    }

    /**
     *
     * @param string $field
     * @param $value
     * @return $this
     */
    public function likeLeft(string $field, $value): Builder
    {
        return $this->where($field, 'likeLeft', $value);
    }

    /**
     *
     * @param string $field
     * @param $value
     * @return $this
     */
    public function likeRight(string $field, $value): Builder
    {
        return $this->where($field, 'likeRight', $value);
    }

    /**
     *
     * @param string $filed
     * @param array $value
     * @return $this
     */
    public function whereBetween(string $filed, array $value): Builder
    {
        return $this->where($filed, 'between', $value);
    }

    /**
     *
     * @param string $filed
     * @param array $value
     * @return $this
     */
    public function whereNotBetween(string $filed, array $value): Builder
    {
        return $this->whereNot($filed, 'between', $value);
    }

    /**
     *
     * @param string $filed
     * @param array $value
     * @return $this
     */
    public function whereBetweenFilter(string $filed, array $value): Builder
    {
        return $this->whereFilter($filed, 'between', $value);
    }

    /**
     *
     * @param $field - 可传闭包或者字段
     * @param string $operate
     * @param null $value
     * @return $this
     */
    public function orWhere($field, string $operate = '', $value = null): Builder
    {
        $this->whereParse($field, $operate, (string)$value, self::SHOULD);
        return $this;
    }

    /**
     *
     * @param $field - 可传闭包或者字段
     * @param string $operate
     * @param null $value
     * @return $this
     */
    public function orWhereNot($field, string $operate = '', $value = null): Builder
    {
        $this->whereParse($field, $operate, (string)$value, self::SHOULD_NOT);
        return $this;
    }

    /**
     * where分析
     * @param $field
     * @param string $operate
     * @param ?string $value
     * @param string $esOrder
     * @return $this
     */
    protected function whereParse($field, string $operate = '', ?string $value = '', string $esOrder = self::FILTER): Builder
    {
        if (is_null($value)) {
            $value = $operate;
            $operate = '=';
        }
        if ($field instanceof \Closure) {
            /** @var Builder $res */
            $res = $field($query = $this->model->newQuery());
            $this->query['bool'][$esOrder][] = $res->query;
            if (in_array($esOrder, [self::SHOULD, self::SHOULD_NOT])) {
                $this->query['bool']['minimum_should_match'] = 1;
            }
        } else {
            $this->parseQuery((string)$field, $operate, $value, $esOrder);
        }
        return $this;
    }

    /**
     * 构建查询参数
     * @param string $field
     * @param string $operate
     * @param $value
     * @param string $esOrder
     * @param bool $isPostFilter
     * @return Builder
     */
    protected function parseQuery(string $field, string $operate, $value, string $esOrder = self::FILTER, bool $isPostFilter = false): Builder
    {
        if (is_null($value)) {
            $value = $operate;
            $operate = '=';
        }
        if (!in_array($operate, $this->operate)) {
            return $this;
        }

        switch ($operate) {
            case '=':
                $result = ['match' => [$field => $value]];
                break;
            case '>':
                $result = ['range' => [$field => ['gt' => $value]]];
                break;
            case '<':
                $result = ['range' => [$field => ['lt' => $value]]];
                break;
            case '>=':
                $result = ['range' => [$field => ['gte' => $value]]];
                break;
            case '<=':
                $result = ['range' => [$field => ['lte' => $value]]];
                break;
            case '<>':
            case '!=':
                $esOrder = sprintf('%s_not', $esOrder);
                $result = ['match' => [$field => $value]];
                break;
            case 'in':
                $result = ['terms' => [$field => $value]];
                break;
            case 'not in':
                $esOrder = sprintf('%s_not', $esOrder);
                $result = ['terms' => [$field => $value]];
                break;
            case 'like':
                // wildcard是可以使用通配符的查询，类似于SQL中like功能
                $result = ['wildcard' => [$field => sprintf('*%s*', $value)]];
                break;
            case 'likeLeft':
                $result = ['wildcard' => [$field => sprintf('%s*', $value)]];
                break;
            case 'likeRight':
                $result = ['wildcard' => [$field => sprintf('*%s', $value)]];
                break;
            case 'regex':
                $result = ['regexp' => [$field => $value]];
                break;
            case 'prefix':
                $result = ['prefix' => [$field => $value]];
                break;
            case 'between':
                $result = ['range' => [$field => ['gte' => $value[0], 'lte' => $value[1]]]];
                break;
            case 'exists':
                // 查询的字段不为空值
                $result = ['exists' => ['field' => $field]];
                break;
            case 'missing':
                $result = ['missing' => ['field' => $field]];
                break;
            case 'match_phrase':
                $result = ['match_phrase' => ['field' => $field]];
                break;
            default:
                $result = [];
                break;
        }

        if ($isPostFilter) {
            // 后置过滤器直接返回 查询条件
            $this->query['post_filter'] = $result;
            return $this;
        }

        $this->query['bool'][$esOrder][] = $result;
        if ($esOrder == self::SHOULD) {
            $this->query['bool']['minimum_should_match'] = 1;
        }
        return $this;
    }

    /**
     * 查询满足条件的数据记录数
     * @return int|mixed
     */
    public function count()
    {
        $this->sql = $this->sqlCombine(0, 0);
        unset($this->sql['from']);
        unset($this->sql['size']);
        unset($this->sql['body']['highlight']);
        unset($this->sql['body']['sort']);
        unset($this->sql['body']['_source']);
        $result = $this->run('count', $this->sql);
        return $result['count'] ?? 0;
    }

    /**
     * 计算平均数，可以多次调用
     * @param string $field
     * @param string $groupField - groupBy方法中的$field
     * @param string $alias
     * @return $this
     */
    public function avg(string $field, string $groupField = '', string $alias = ''): Builder
    {
        return $this->aggsOperate($field, $groupField, $alias, 'avg');
    }

    /**
     * 计算最小值，可多次调用
     * @param string $field
     * @param string $groupField
     * @param string $alias
     * @return $this
     */
    public function min(string $field, string $groupField = '', string $alias = ''): Builder
    {
        return $this->aggsOperate($field, $groupField, $alias, 'min');
    }

    /**
     * 计算最大值,可多次调用
     * @param string $field
     * @param string $groupField
     * @param string $alias
     * @return $this
     */
    public function max(string $field, string $groupField = '', string $alias = ''): Builder
    {
        return $this->aggsOperate($field, $groupField, $alias, 'max');
    }

    /**
     * 计算总和,可多次调用
     * @param string $field
     * @param string $groupField
     * @param string $alias
     * @return $this
     */
    public function sum(string $field, string $groupField = '', string $alias = ''): Builder
    {
        return $this->aggsOperate($field, $groupField, $alias, 'sum');
    }

    /**
     * 聚合公共操作
     * @param string $field - 需要聚合的字段
     * @param string $groupField - 如果配合groupBy使用，此字段为groupBy的field，默认全局桶搜索all
     * @param string $alias - 别名操作
     * @param string $operate - 聚合操作 count, avg, max , min, max
     * @return $this
     */
    private function aggsOperate(string $field, string $groupField = 'all', string $alias = '', string $operate = ''): Builder
    {
        $groupField = str_replace('aggs_', '', $groupField);
        $groupField = $groupField ? sprintf('aggs_%s', $groupField) : '';
        $aggColumn = $alias ?: sprintf('%s_%s', $operate, $field);
        if (!empty($groupField)) {
            $this->query[self::TYPE_AGGS][$groupField][self::TYPE_AGGS][$aggColumn] = [
                $operate => [
                    'field' => $field
                ]
            ];
        } else {
            $this->query[self::TYPE_AGGS][$aggColumn] = [
                $operate => [
                    'field' => $field
                ]
            ];
        }
        return $this;
    }

    /**
     * 执行聚合操作
     * @return Collection
     */
    public function aggsExecute(): Collection
    {
        $this->sql = $this->sqlCombine(0, 0, self::TYPE_AGGS);
        $result = $this->run('search', $this->sql);
        $original = $result['aggregations'] ?? [];
        return Collection::make($original)->map(function ($value) {
            $attributes = $value['buckets'] ?? ($value ?: []);
            $model = $this->model->newInstance();
            $model->setAttributes($attributes);
            $model->setOriginal($value);
            return $model;
        });
    }
}
