<?php

declare(strict_types=1);

namespace Eline\ElasticSearch\Pool;;

trait HasAttributes
{
    /**
     * The model's attributes.
     *
     * @var array
     */
    private array $attributes = [];

    /**
     * The model attribute's original state.
     *
     * @var array
     */
    private array $original = [];
    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected array $casts = [];

    /**
     * The built-in, primitive cast types supported.
     *
     * @var array
     */
    protected static array $primitiveCastTypes = [
        'array',
        'bool',
        'boolean',
        'collection',
        'custom_datetime',
        'date',
        'datetime',
        'decimal',
        'double',
        'float',
        'int',
        'integer',
        'json',
        'object',
        'real',
        'string',
        'timestamp',
    ];

    /**
     * 初始化数据
     * @return void
     */
    protected function initData() :void
    {
        $this->setOriginal([]);
        $this->setAttributes([]);
    }
    /**
     *
     * @return array
     */
    public function getAttributes(): array
    {
        return $this->attributes;
    }

    /**
     *
     * @param array $attributes
     */
    public function setAttributes(array $attributes): void
    {
        $this->attributes = $attributes;
    }

    /**
     *
     * @return array
     */
    public function getOriginal(): array
    {
        return $this->original;
    }

    /**
     *
     * @param array $original
     */
    public function setOriginal(array $original): void
    {
        $this->original = $original;
    }

    /**
     *
     * @return array
     */
    public function getCasts(): array
    {
        return $this->casts;
    }

    /**
     *
     * @param array $casts
     */
    public function setCasts(array $casts): void
    {
        $this->casts = $casts;
    }

    /**
     *
     * @return array
     */
    public function toArray(): array
    {
        return $this->getAttributes();
    }

    /**
     * Convert the object into something JSON serializable.
     * @return array
     */
    public function jsonSerialize(): array
    {
        return $this->toArray();
    }

    /**
     * Convert the object to its JSON representation.
     * @param int $options
     * @return string
     */
    public function toJson(int $options = 0): string
    {
        return json_encode($this->jsonSerialize(), $options);
    }

    /**
     * Convert the model to its string representation.
     * @return string
     */
    public function __toString(): string
    {
        return $this->toJson();
    }
}
