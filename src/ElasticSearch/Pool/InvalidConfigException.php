<?php

declare(strict_types=1);

namespace Eline\ElasticSearch\Pool;;

class InvalidConfigException extends \RuntimeException
{
}
