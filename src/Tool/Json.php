<?php

namespace Eline\Tool;

use http\Exception\InvalidArgumentException;

class Json
{
    /**
     * json编码
     * @param $data
     * @param int $options
     * @return false|string
     */
    public static function encode($data, int $options = JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES)
    {
        $json = json_encode($data, $options);
        static::handleJsonError(json_last_error(), json_last_error_msg());
        return $json;
    }

    /**
     * json解码
     * @param $json
     * @param bool $assoc
     * @return mixed
     */
    public static function decode($json, bool $assoc = true)
    {
        $decode = json_decode($json, $assoc);
        static::handleJsonError(json_last_error(), json_last_error_msg());
        return $decode;
    }

    /**
     * 统一处理json异常报错
     * @param $lastError
     * @param $message
     */
    protected static function handleJsonError($lastError, $message)
    {
        if ($lastError === JSON_ERROR_NONE) {
            return;
        }
        throw new InvalidArgumentException($message, $lastError);
    }
}