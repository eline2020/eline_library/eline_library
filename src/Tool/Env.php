<?php

namespace Eline\Tool;

use ArrayAccess;
use Exception;

class Env implements ArrayAccess
{
    /**
     * 环境变量数据
     * @var array
     */
    protected array $data = [];

    /**
     * Env对象实例
     * @var Env|null
     */
    protected static ?Env $instance = null;

    /**
     * 键名修改成大写或者小写
     * @var int
     */
    protected int $case;

    /**
     * Env constructor
     */
    protected function __construct()
    {
        // 设置空数组
        $this->data = $_ENV;
    }

    /**
     * 单例模式
     * @return Env|null
     */
    public static function getInstance(): ?Env
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * 加载文件配置
     * @param string $file
     * @param int $case
     */
    public function load(string $file, int $case = CASE_LOWER)
    {
        // 设置键名大小写
        $this->case = in_array($case, [CASE_LOWER, CASE_UPPER]) ? $case : CASE_LOWER;

        // 解析一个配置文件
        $env = parse_ini_file($file, true);
        $this->set($env);
    }

    /**
     * 设置配置
     * @param $env
     * @param null $value
     */
    public function set($env, $value = null)
    {
        if (is_array($env)) {
            $this->multiArrayEnv($env, $this->data);
        } else {
            $this->valueFix($env, $value, $this->data);
        }
    }

    /**
     * 多维数组配置
     * @param $env
     * @param $data
     */
    protected function multiArrayEnv($env, &$data)
    {
        // 将数组中的所有键名修改为全大写或小写(CASE_UPPER、CASE_LOWER)
        $env = array_change_key_case($env, $this->case);
        foreach ($env as $key => $val) {
            $this->valueFix($key, $val, $data);
        }
    }

    /**
     * 设置值
     * @param $key
     * @param $val
     * @param $data
     */
    protected function valueFix($key, $val, &$data)
    {
        $key = trim(strval($key));
        if (strpos($key, '.') !== false) {
            $keyArr = explode('.', $key);
            $len = count($keyArr);
            $lastKeyIndex = $len - 1;
            (!isset($data[$keyArr[0]]) || !is_array($data[$keyArr[0]])) && $data[$keyArr[0]] = [];
            $currentElem = &$data[$keyArr[0]];
            for ($i = 1; $i < $len; $i++) {
                $elemKey = trim($keyArr[$i]);
                if ($i == $lastKeyIndex) {
                    if (is_array($val)) {
                        $this->multiArrayEnv($val, $currentElem);
                    } else {
                        $currentElem[$elemKey] = $val;
                    }
                } else {
                    !isset($currentElem[$elemKey]) && $currentElem[$elemKey] = [];
                    $currentElem = &$currentElem[$elemKey];
                }
            }
        } elseif (is_array($val)) {
            $data[$key] = [];
            $this->multiArrayEnv($val, $data[$key]);
        } else {
            $data[$key] = $val;
        }
    }

    /**
     * 获取环境变量
     * @param null $paramsKey
     * @param null $default
     * @param false $phpPrefix
     * @return array|bool|mixed|string|null
     */
    public function get($paramsKey = null, $default = null, bool $phpPrefix = false)
    {
        // 获取全部环境变量
        if (is_null($paramsKey)) return $this->data;

        // 将键全部转成大写或者小写
        if ($this->case == CASE_LOWER) {
            $paramsKey = strtolower($paramsKey);
        } else {
            $paramsKey = strtoupper($paramsKey);
        }
        $splitNameArr = explode('|', $paramsKey);

        foreach ($splitNameArr as $name) {
            if (isset($this->data[$name])) return $this->data[$name];
            if (strpos($name, '.') !== false) {
                $subNameArr = explode('.', $name);
                $isFind = true;
                $findValue = $this->data;
                foreach ($subNameArr as $key) {
                    $key = trim($key);
                    if (isset($findValue[$key])) {
                        $findValue = $findValue[$key];
                    } else {
                        $isFind = false;
                        break;
                    }
                }
                if ($isFind) return $findValue;
            }

            // 获取一个环境变量的值
            $phpPrefix && $name = 'PHP_' . $name;
            $result = getenv($name);

            // 结果为false则跳过
            if ($result === false) continue;
            if ($result === 'false') {
                return false;
            } elseif ($result === 'true') {
                return true;
            }
            return $result;
        }
        return $default;
    }

    /**
     * 检测是否存在环境变量
     * @param $name
     * @return bool
     */
    public function has($name): bool
    {
        return !is_null($this->get($name));
    }

    /**
     * 设置环境变量
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        $this->set($name, $value);
    }

    /**
     * 获取环境变量
     * @param $name
     * @return array|bool|mixed|string|null
     */
    public function __get($name)
    {
        return $this->get($name);
    }

    /**
     * 检测是否存在环境变量
     * @param $name
     * @return bool
     */
    public function __isset($name)
    {
        return $this->has($name);
    }

    /**
     * ArrayAccess
     * @param mixed $offset
     * @param mixed $value
     */
    public function offsetSet($offset, $value)
    {
        // TODO: Implement offsetSet() method.
        $this->set($offset, $value);
    }

    /**
     * ArrayAccess
     * @param mixed $offset
     * @return array|bool|mixed|string|null
     */
    public function offsetGet($offset)
    {
        // TODO: Implement offsetGet() method.
        return $this->get($offset);
    }

    /**
     * ArrayAccess
     * @param mixed $offset
     * @return bool
     */
    public function offsetExists($offset): bool
    {
        // TODO: Implement offsetExists() method.
        return $this->has($offset);
    }

    /**
     * ArrayAccess
     * @param mixed $offset
     * @throws Exception
     */
    public function offsetUnset($offset)
    {
        // TODO: Implement offsetUnset() method.
        throw new Exception('not support: unset');
    }
}