<?php

namespace Eline\Tool;

class Config
{
    /**
     * 环境变量数据
     * @var array
     */
    protected array $data = [];

    /**
     * Env对象实例
     * @var Config|null
     */
    protected static ?Config $instance = null;

    /**
     * Env constructor
     */
    protected function __construct()
    {
        // 设置空数组
        $this->data = $_ENV;
    }

    /**
     * 单例模式
     * @return Config|null
     */
    public static function getInstance(): ?Config
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * 加载配置文件
     * @return void
     */
    public function load()
    {
        // 获取配置文件
        $configInfos = $this->getConfigFiles(CONFIG_PATH);
        // 设置配置数据
        $this->data = $this->setConfigValue($configInfos);
    }

    /**
     * @param string $key
     * @param $value
     * @param bool $isReplace
     */
    public function set(string $key, $value, bool $isReplace = false)
    {
        $keyArr = explode('.', $key);
        if (!$this->has($key) || $isReplace) {
            $this->setArrayValue($this->data, $keyArr, $value);
        }
    }

    /**
     * 获取配置
     * @param string $key
     * @return mixed
     */
    public function get(string $key)
    {
        $keyArr = explode('.', $key);
        $data = [];
        foreach ($keyArr as $index => $field) {
            // 减少内存
            if ($index === 0) {
                $data = $this->data[$field] ?? [];
            } else {
                $data = $data[$field] ?? [];
            }
            if (empty($data)) return [];
        }
        return $data;
    }

    /**
     * 是否存在
     * @param string $key
     * @return bool
     */
    public function has(string $key): bool
    {
        $keyArr = explode('.', $key);
        $data = [];
        foreach ($keyArr as $index => $field) {
            // 减少内存
            if ($index === 0 && isset($this->data[$field])) {
                $data = $this->data[$field];
            } elseif (isset($data[$field])) {
                $data = $data[$field];
            } else {
                return false;
            }
        }
        return true;
    }

    /**
     * 目录与文件排序(先文件后目录)
     * @param string $directory
     * @return array
     */
    private function getConfigFiles(string $directory): array
    {
        $configInfos = [];
        $files = scandir($directory);
        $this->fileDirectorySort($directory, $files);
        foreach ($files as $file) {
            if (in_array($file, ['.', '..'])) continue;
            $filePath = $directory . '/' . $file;
            // 判断是否为目录
            if (is_dir($filePath)) {
                // 如果是目录，则递归调用getConfigFiles()函数获取嵌套的PHP文件
                $nestedPhpFiles = $this->getConfigFiles($filePath);
                $configInfos[$file] = $nestedPhpFiles;
            } elseif (pathinfo($file, PATHINFO_EXTENSION) === 'php') {
                // 如果是PHP文件，则添加到结果数组中
                $configInfos[] = $filePath;
            }
        }
        return $configInfos;
    }

    /**
     * 文件路径排序
     * @param string $directory
     * @param array $files
     * @return void
     */
    private function fileDirectorySort(string $directory, array &$files)
    {
        // 对文件和子目录进行排序，先按文件名排序，再按目录名排序
        usort($files, function ($a, $b) use ($directory) {
            $fileA = $directory . '/' . $a;
            $fileB = $directory . '/' . $b;
            if (is_file($fileA) && is_file($fileB)) {
                // 两个都是文件，按文件名排序
                return strcmp($a, $b);
            } elseif (is_file($fileA)) {
                // $a是文件，$b是目录，将$a排在前面
                return -1;
            } elseif (is_file($fileB)) {
                // $b是文件，$a是目录，将$b排在前面
                return 1;
            } else {
                // 两个都是目录，按目录名排序
                return strcmp($a, $b);
            }
        });
    }

    /**
     * 设置配置数据
     * @param array $configInfos
     * @return array
     */
    private function setConfigValue(array $configInfos): array
    {
        $data = [];
        foreach ($configInfos as $index => $filePath) {
            if (is_string($filePath)) {
                $value = require_once($filePath);
                $filename = pathinfo($filePath, PATHINFO_FILENAME);
                $data[$filename] = array_merge($data, $value);
            } else {
                $value = $this->setConfigValue($filePath);
                $data[$index] = $value;
            }
        }
        return $data;
    }


    /**
     * 设置配置数据
     * @param $array
     * @param $keys
     * @param $value
     * @return void
     */
    private function setArrayValue(&$array, $keys, $value)
    {
        // 取出第一个键名
        $key = array_shift($keys);
        // 如果键名数组为空，表示已经到达最后一个键名
        if (empty($keys)) {
            // 直接设置值
            $array[$key] = $value;
        } else {
            // 如果当前键名不存在，则创建一个新数组
            if (!isset($array[$key])) {
                $array[$key] = array();
            }
            // 递归调用，继续设置下一个键名的值
            $this->setArrayValue($array[$key], $keys, $value);
        }
    }
}
