<?php

namespace Eline\Tool;

class PackageParser
{
    /**
     * 编码包
     * @param array $data
     * @return string
     */
    public static function encode(array $data): string
    {
        $data = Json::encode($data);
        return pack('N', strlen($data)) . $data;
    }

    /**
     * 解码包
     * @param $str
     * @return mixed
     */
    public static function decode($str)
    {
        $data = unpack('N', $str);
        $len = $data[1];
        $str = substr($str, -$len);
        return Json::decode($str);
    }
}