<?php

namespace Eline\Logger;

use Hyperf\Context\Context;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class CoroutineHandler extends StreamHandler
{
    /**
     * 文件全路径名称
     * @var string
     */
    protected string $filename;

    /**
     * 允许日志最大子目录数量
     * @var int
     */
    protected int $maxDirs;

    /**
     * CoroutineHandler constructor.
     * @param string $filename
     * @param int $maxDirs
     * @param int $level
     * @param bool $bubble
     * @param int|null $filePermission
     * @param bool $useLocking
     */
    public function __construct(string $filename, int $maxDirs, $level = Logger::DEBUG, bool $bubble = true, ?int $filePermission = null, bool $useLocking = false)
    {
        $this->filename = $filename;
        $this->maxDirs = $maxDirs;
        parent::__construct($this->getFormattedFilename(), $level, $bubble, $filePermission, $useLocking);
    }

    /**
     * 重写父类处理方法
     * @param array $record
     * @return bool
     */
    public function handle(array $record): bool
    {
        if (!$this->isHandling($record)) {
            return false;
        }

        if ($this->processors) {
            $record = $this->processRecord($record);
        }

        $id = $this->getLogId();
        $records = [];
        if (Context::has($id)) {
            $records = Context::get($id);
        }

        if (empty($records)) {
            defer(function () use ($id) {
                $content = Context::get($id);
                $this->write(['formatted' => implode("", $content) . PHP_EOL]);
                Context::set($id, []);
            });
        }

        array_push($records, $this->getFormatter()->format($record));
        unset($record);
        Context::set($id, $records);

        return false === $this->bubble;
    }

    protected function write(array $record): void
    {
        $this->checkLogSize();
        parent::write($record);
        $this->checkDirNum();
    }

    /**
     * 获取记录log id
     * @return string
     */
    protected function getLogId(): string
    {
        return "log_formatted";
    }

    /**
     * 获取格式化后的文件全名
     * @return string
     */
    protected function getFormattedFilename(): string
    {
        $fileInfo = pathinfo($this->filename);
        $filename = $fileInfo['dirname'] . '/' . date('Ymd') . '/' . $fileInfo['filename'];
        if (!empty($fileInfo['extension'])) {
            $filename .= '.' . $fileInfo['extension'];
        }

        return $filename;
    }

    /**
     * 检查日志大小，超过2M，则重命名
     */
    protected function checkLogSize()
    {
        clearstatcache();
        $this->close();
        $filename = $this->getFormattedFilename();
        if ($this->url != $filename) {
            $this->url = $filename;
        }

        $fileInfo = pathinfo($this->url);
        if (is_file($this->url) && floor(2097152) <= filesize($this->url)) {
            $newFilename = $fileInfo['dirname'] . '/' . date('His') . '-' . $fileInfo['filename'];
            if (!empty($fileInfo['extension'])) {
                $newFilename .= '.' . $fileInfo['extension'];
            }

            @rename($this->url, $newFilename);
        }
    }

    /**
     * 检查目录数量，超限则删除它
     */
    protected function checkDirNum()
    {
        $fileInfo = pathinfo($this->filename);
        $dirs = glob($fileInfo['dirname'] . '/[0-9][0-9][0-9][0-9]*');
        if ($this->maxDirs == 0 || $this->maxDirs >= count($dirs)) {
            return;
        }

        usort($dirs, function ($a, $b) {
            return strcmp($b, $a);
        });

        foreach (array_slice($dirs, $this->maxDirs) as $dir) {
            if (is_dir($dir)) {
                set_error_handler(function (int $errno, string $errstr, string $errfile, int $errline): bool {
                    return false;
                });

                @array_map('unlink', glob($dir . '/*'));
                @rmdir($dir);
                restore_error_handler();
            }
        }
    }
}