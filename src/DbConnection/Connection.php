<?php

namespace Eline\DbConnection;

use Eline\DbConnection\Pool\DbPool;
use Hyperf\Contract\ConnectionInterface;
use Hyperf\Contract\StdoutLoggerInterface;
use Hyperf\Logger\LoggerFactory;
use Hyperf\Pool\Connection as BaseConnection;
use Hyperf\Pool\Exception\ConnectionException;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use think\db\ConnectionInterface as DbConnectionInterface;
use Psr\SimpleCache\CacheInterface;

class Connection extends BaseConnection implements ConnectionInterface, DbConnectionInterface
{
    use DbConnection;

    /**
     * 数据库实例对象
     * @var DbPool
     */
    protected $pool;

    /**
     * 数据库连接对象
     * @var DbConnectionInterface
     */
    protected DbConnectionInterface $connection;

    /**
     *
     * @var DbManager|mixed
     */
    protected $db;

    /**
     * 配置
     * @var mixed
     */
    protected $config;

    /**
     * 日志
     * @var StdoutLoggerInterface|mixed
     */
    protected $logger;

    /**
     *
     * @var bool
     */
    protected bool $transaction = false;

    /**
     * 构造函数
     * @param ContainerInterface $container
     * @param DbPool $pool
     * @param array $config
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __construct(ContainerInterface $container, DbPool $pool, array $config)
    {
        parent::__construct($container, $pool);
        $cache = $container->get(CacheInterface::class);
        $log = $container->get(LoggerFactory::class)->get('app');
        $this->db = $container->get(DbManager::class);
        $this->db->setConfig($config);
        $this->db->setCache($cache);
        $this->db->setLog($log);
        $this->logger = $container->get(StdoutLoggerInterface::class);
        $this->reconnect();
    }

    /**
     * 魔术方法(方法不存在时调用)
     * @param $name
     * @param $arguments
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        return $this->connection->{$name}(...$arguments);
    }

    /**
     * 获取活动连接
     * @return $this
     * @throws ConnectionException
     */
    public function getActiveConnection(): self
    {
        if ($this->check()) {
            return $this;
        }
        if (! $this->reconnect()) {
            throw new ConnectionException('Connection reconnect failed.');
        }
        return $this;
    }

    /**
     * 重新连接
     * @return bool
     */
    public function reconnect(): bool
    {
        $this->close();
        $this->connection = $this->db->connect($this->pool->getName(), true);
        $this->lastUseTime = microtime(true);
        return true;
    }

    /**
     * 关闭连接
     * @return bool
     */
    public function close(): bool
    {
        if (!empty($this->connection) && $this->connection instanceof DbConnectionInterface) {
            try {
                $this->connection->close();
            } catch (\Exception $exception) {
                // 暂时不处理
            }
        }
        unset($this->connection);
        return true;
    }

    /**
     * 释放连接
     * @return void
     */
    public function release(): void
    {
        if (!empty($this->connection) && $this->connection instanceof DbConnectionInterface) {
            // 在连接释放到池子之前，处理一些逻辑...
        }
        if ($this->isTransaction()) {
            $this->rollBack();
            $this->logger->error('Maybe you\'ve forgotten to commit or rollback the MySQL transaction.');
        }
        parent::release();
    }

    /**
     * 设置事务
     * @param bool $transaction
     * @return void
     */
    public function setTransaction(bool $transaction): void
    {
        $this->transaction = $transaction;
    }

    /**
     * 盘点是否开启事务
     * @return bool
     */
    public function isTransaction(): bool
    {
        return $this->transaction;
    }
}