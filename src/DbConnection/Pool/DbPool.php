<?php

namespace Eline\DbConnection\Pool;

use Eline\DbConnection\Connection;
use Eline\DbConnection\ConnectionResolver;
use Hyperf\Contract\ConfigInterface;
use Hyperf\Contract\ConnectionInterface;
use Hyperf\Pool\Pool;
use Hyperf\Utils\Arr;
use InvalidArgumentException;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class DbPool extends Pool
{
    /**
     * 数据库连接名
     * @var string
     */
    protected string $name;

    /**
     * 数据库配置
     * @var mixed
     */
    protected $config;

    /**
     * 构造函数
     * @param ContainerInterface $container
     * @param string $name
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __construct(ContainerInterface $container, string $name)
    {
        $this->name = $name;
        $config = $container->get(ConfigInterface::class);
        // 获取数据库配置
        $key = make(ConnectionResolver::class)->getContextKey($this->name);
        if (!$config->has($key)) {
            throw new InvalidArgumentException('config[%s] is not exist!', $key);
        }
        $config->set($key . '.name', $name);
        $this->config = $config->get('databases');
        $options = Arr::get($config->get($key), 'pool', []);
        parent::__construct($container, $options);
    }

    /**
     * 获取连接名
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * 创建连接
     * @return ConnectionInterface
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    protected function createConnection(): ConnectionInterface
    {
        return new Connection($this->container, $this, $this->config);
    }
}