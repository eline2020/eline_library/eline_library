<?php

declare(strict_types=1);

namespace Eline\DbConnection;

use Psr\SimpleCache\CacheInterface;
use think\db\BaseQuery;
use think\DbManager;

trait DbConnection
{
    /**
     * 获取查询对象
     * @return string
     */
    public function getQueryClass(): string
    {
        return $this->__call(__FUNCTION__, func_get_args());
    }

    /**
     * 设置表
     * @param $table
     * @return mixed
     */
    public function table($table)
    {
        return $this->__call(__FUNCTION__, func_get_args());
    }

    /**
     * @param $name
     * @return mixed
     */
    public function name($name)
    {
        return $this->__call(__FUNCTION__, func_get_args());
    }

    /**
     * @param array $config
     * @param int $linkNum
     * @return mixed
     */
    public function connect(array $config = [], $linkNum = 0)
    {
        return $this->__call(__FUNCTION__, func_get_args());
    }

    /**
     * @param DbManager $db
     */
    public function setDb(DbManager $db)
    {
        $this->__call(__FUNCTION__, func_get_args());
    }

    /**
     * 设置缓存
     * @param CacheInterface $cache
     */
    public function setCache(CacheInterface $cache)
    {
        $this->__call(__FUNCTION__, func_get_args());
    }

    /**
     * 获取配置
     * @param string $config
     * @return mixed
     */
    public function getConfig(string $config = '')
    {
        return $this->__call(__FUNCTION__, func_get_args());
    }

    /**
     * 查询一行
     * @param BaseQuery $query
     * @return array
     */
    public function find(BaseQuery $query): array
    {
        return $this->__call(__FUNCTION__, func_get_args());
    }

    /**
     * 查询一行或者多行
     * @param BaseQuery $query
     * @return array
     */
    public function select(BaseQuery $query): array
    {
        return $this->__call(__FUNCTION__, func_get_args());
    }

    /**
     * 插入一行
     * @param BaseQuery $query
     * @param bool $getLastInsID
     * @return mixed
     */
    public function insert(BaseQuery $query, bool $getLastInsID = false)
    {
        return $this->__call(__FUNCTION__, func_get_args());
    }

    /**
     * 插入一行或多行
     * @param BaseQuery $query
     * @param array $dataSet
     * @return int
     */
    public function insertAll(BaseQuery $query, array $dataSet = []): int
    {
        return $this->__call(__FUNCTION__, func_get_args());
    }

    /**
     * 更新数据
     * @param BaseQuery $query
     * @return int
     */
    public function update(BaseQuery $query): int
    {
        return $this->__call(__FUNCTION__, func_get_args());
    }

    /**
     * 删除数据
     * @param BaseQuery $query
     * @return int
     */
    public function delete(BaseQuery $query): int
    {
        return $this->__call(__FUNCTION__, func_get_args());
    }

    /**
     * 获取一列
     * @param BaseQuery $query
     * @param string $field
     * @param null $default
     * @return mixed
     */
    public function value(BaseQuery $query, string $field, $default = null)
    {
        return $this->__call(__FUNCTION__, func_get_args());
    }

    /**
     * @param BaseQuery $query
     * @param $column
     * @param string $key
     * @return array
     */
    public function column(BaseQuery $query, $column, string $key = ''): array
    {
        return $this->__call(__FUNCTION__, func_get_args());
    }

    /**
     * @param callable $callback
     * @return mixed
     */
    public function transaction(callable $callback)
    {
        return $this->__call(__FUNCTION__, func_get_args());
    }

    /**
     * 开启事务
     * @return void
     */
    public function startTrans()
    {
        $this->setTransaction(true);
        $this->__call(__FUNCTION__, func_get_args());
    }

    /**
     * 提交事务
     * @return void
     */
    public function commit()
    {
        $this->setTransaction(false);
        $this->__call(__FUNCTION__, func_get_args());
    }

    /**
     * 回滚事务
     * @return void
     */
    public function rollback()
    {
        $this->setTransaction(false);
        $this->__call(__FUNCTION__, func_get_args());
    }

    /**
     * 获取最后执行的SQL
     * @return string
     */
    public function getLastSql(): string
    {
        return $this->__call(__FUNCTION__, func_get_args());
    }
}