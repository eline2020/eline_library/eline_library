<?php

namespace Eline\DbConnection;

use Hyperf\Di\Exception\NotFoundException;
use Hyperf\Utils\ApplicationContext;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use think\db\ConnectionInterface;

/**
 * Class Db
 * @package App\DbConnection
 * @method static connection($arguments)
 */
class Db
{
    /**
     * 对象注入
     * @var ContainerInterface
     */
    protected ContainerInterface $container;

    /**
     * 构造函数
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * 魔术方法(方法不存在时调用)
     * @param $name
     * @param $arguments
     * @return ConnectionInterface|null
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __call($name, $arguments)
    {
        $db = ApplicationContext::getContainer()->get(Db::class);
        if ($name === 'connection') {
            return $db->__connection(...$arguments);
        }
        return $db->__connection()->{$name}(...$arguments);
    }

    /**
     * 魔术方法(静态方法不存在时调用)
     * @param $name
     * @param $arguments
     * @return ConnectionInterface|null
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function __callStatic($name, $arguments)
    {
        $db = ApplicationContext::getContainer()->get(Db::class);
        if ($name === 'connection') {
            return $db->__connection(...$arguments);
        }
        return $db->__connection()->{$name}(...$arguments);
    }

    /**
     * 获取连接对象
     * @param string $pool
     * @return ConnectionInterface|null
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws NotFoundException
     */
    private function __connection(string $pool = 'default'): ?ConnectionInterface
    {
        $resolver = $this->container->get(ConnectionResolver::class);
        return $resolver->connection($pool);
    }
}