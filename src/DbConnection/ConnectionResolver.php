<?php

namespace Eline\DbConnection;

use Eline\DbConnection\Pool\PoolFactory;
use Hyperf\Context\Context;
use Hyperf\Di\Exception\NotFoundException;
use Hyperf\Utils\Coroutine;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use think\db\ConnectionInterface;

class ConnectionResolver
{
    /**
     * 数据库连接名称
     * @var string
     */
    protected string $default = 'default';

    /**
     * 工厂
     * @var PoolFactory|mixed
     */
    protected $factory;

    /**
     * 注入对象
     * @var ContainerInterface
     */
    protected ContainerInterface $container;

    /**
     * 构造函数
     * @param ContainerInterface $container
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->factory = $container->get(PoolFactory::class);
    }

    /**
     * 获取数据库连接对象
     * @param null $name
     * @return ConnectionInterface
     * @throws ContainerExceptionInterface
     * @throws NotFoundException
     * @throws NotFoundExceptionInterface
     */
    public function connection($name = null): ?ConnectionInterface
    {
        if (is_null($name)) {
            $name = $this->getDefaultConnection();
        }

        $connection = null;
        $id = $this->getContextKey($name);
        if (Context::has($id)) {
            $connection = Context::get($id);
        }

        if (! $connection instanceof ConnectionInterface) {
            $pool = $this->factory->getPool($name);
            $connection = $pool->get();
            try {
                $connection = $connection->getConnection();
                Context::set($id, $connection);
            } finally {
                if (Coroutine::inCoroutine()) {
                    defer(function () use ($connection, $id) {
                        Context::set($id, null);
                        $connection->release();
                    });
                }
            }
        }

        return $connection;
    }

    /**
     * 获取默认数据库连接名称
     * @return string
     */
    public function getDefaultConnection(): string
    {
        return $this->default;
    }

    /**
     * 设置默认数据库连接名称
     * @param string $name
     * @return void
     */
    public function setDefaultConnection(string $name)
    {
        $this->default = $name;
    }

    /**
     * The key to identify the connection object in coroutine context.
     * @param mixed $name
     */
    public function getContextKey($name): string
    {
        return sprintf('databases.connections.%s', $name);
    }
}