<?php

namespace Eline\Exception;

use Exception;
use Throwable;

class BusinessException extends Exception
{
    /**
     * 返回数据
     * @var array
     */
    protected array $data = [];

    /**
     * BusinessException constructor
     * @param string $message
     * @param array $params
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message = '', array $params = [], $code = 1000, Throwable $previous = null)
    {
        $this->data = $params;
        parent::__construct($message, $code, $previous);
    }

    /**
     * 获取数据
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }
}
