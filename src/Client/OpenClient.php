<?php

namespace Eline\Client;

use Exception;

class OpenClient
{
    /**
     * 执行超时30秒
     * @var int
     */
    private int $curlRequestTimeout = 30;

    /**
     * 连接超时5秒
     * @var int
     */
    private int $curlConnectTimeout = 5;

    /**
     * 加密Key
     * @var string
     */
    private string $encryptKey = '';

    /**
     * 连接的实例
     * @var OpenClient|null
     */
    protected static ?OpenClient $instance = null;

    /**
     * OpenClient constructor
     * @param string $encryptKey
     * @param null $curlRequestTimeout
     * @param null $curlConnectTimeout
     */
    private function __construct(string $encryptKey, $curlRequestTimeout = null, $curlConnectTimeout = null)
    {
        $this->encryptKey = $encryptKey;
        !empty($curlRequestTimeout) && $this->curlRequestTimeout = intval($curlRequestTimeout);
        !empty($curlConnectTimeout) && $this->curlConnectTimeout = intval($curlConnectTimeout);
    }

    /**
     * 实例化OpenClient Object
     * @param $encryptKey
     * @param null $curlRequestTimeout
     * @param null $curlConnectTimeout
     * @return OpenClient
     */
    public static function getInstance($encryptKey, $curlRequestTimeout = null, $curlConnectTimeout = null)
    {
        if (!self::$instance) {
            self::$instance = new self($encryptKey, $curlRequestTimeout, $curlConnectTimeout);
        }
        return self::$instance;
    }

    /**
     * GET请求
     * @param $url
     * @param array $data
     * @return array|bool
     * @throws Exception
     */
    public function get($url, array $data = [])
    {
        !empty($data) && $url .= '?' . http_build_query($data);
        return $this->callCurl($url, 'GET');
    }

    /**
     * POST请求
     * @param $url
     * @param array $data
     * @return array|bool
     * @throws Exception
     */
    public function post($url, array $data = [])
    {
        return $this->callCurl($url, 'POST', $data);
    }

    /**
     * PUT请求
     * @param $url
     * @param array $data
     * @return array|bool
     * @throws Exception
     */
    public function put($url, array $data = [])
    {
        return $this->callCurl($url, 'PUT', $data);
    }

    /**
     * PATCH请求
     * @param $url
     * @param array $data
     * @return array|bool
     * @throws Exception
     */
    public function patch($url, array $data = [])
    {
        return $this->callCurl($url, 'PATCH', $data);
    }

    /**
     * DELETE请求
     * @param $url
     * @param array $data
     * @return array|bool
     * @throws Exception
     */
    public function delete($url, array $data = [])
    {
        return $this->callCurl($url, 'DELETE', $data);
    }

    /**
     * @param $url
     * @param $method
     * @param array $data
     * @return array|bool
     * @throws Exception
     */
    public function callCurl($url, $method, array $data = [])
    {
        if (!function_exists('curl_init')) {
            throw new Exception('missing curl extend');
        }

        $ch = curl_init();

        // Encrypt headers
        $headers = $this->getEncryptionHeaders();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        // set ua (避免AWS#AWSManagedRulesCommonRuleSet#NoUserAgent_HEADER)
        curl_setopt($ch, CURLOPT_USERAGENT, 'curl-eline/1.0.0');

        //set ssl options
        if (stripos($url, 'https://') !== false) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
            curl_setopt($ch, CURLOPT_SSLVERSION, 1);
        }

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, $this->curlRequestTimeout); // 执行超时
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $this->curlConnectTimeout); // 连接超时

        if ($method == 'POST') {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        } elseif (in_array($method, ['PUT', 'PATCH', 'DELETE'])) {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        }

        return $this->__exec($ch);
    }

    /**
     * @param $ch
     * @return bool|array
     * @throws Exception
     */
    private function __exec($ch)
    {
        $output = curl_exec($ch);
        $curlErrorNo = curl_errno($ch) ? curl_errno($ch) : null;
        $curlError = curl_errno($ch) ? curl_error($ch) : null;
        $status = curl_getinfo($ch);
        curl_close($ch);
        if ($output === false) {
            throw new Exception('network error, curl_errno: $curlErrorNo, curl_error: $curlError');
        }

        if (intval($status['http_code']) != 200) {
            throw new Exception('unexpected http code ' . intval($status['http_code']));
        }

        $respData = json_decode($output, true);

        if (!empty($respData) && is_array($respData) && isset($respData['code']) && $respData['code'] != 0) {
            throw new Exception($respData['msg']);
        }
        return $respData;
    }

    /**
     * 生成加密头信息
     * @return array
     */
    private function getEncryptionHeaders(): array
    {
        $reTime = md5(uniqid(microtime(true), true));
        $reToken = md5($reTime . $this->encryptKey);

        return [
            're-time:' . $reTime,
            're-token:' . $reToken
        ];
    }
}
