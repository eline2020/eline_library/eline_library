<?php

namespace Eline\Client;

use Eline\Exception\BusinessException;
use Eline\Tool\PackageParser;
use Exception;
use Swoole\Client;

class TcpClient
{
    /**
     * 盐值
     * @var string
     */
    const SALT_VALUE = 'TGin83vR';

    /**
     * 版本
     * @var string
     */
    const VERSION = '0.9';

    /**
     * swoole客户端
     * @var null|Client
     */
    protected ?Client $client = null;

    /**
     * 请求数据
     * @var array
     */
    public array $data = [];

    /**
     * 连接服务器实例
     * @var array
     */
    protected static array $instance = [];

    /**
     * TcpClient constructor
     * @param $serviceName
     * @param $config
     * @return void
     * @throws Exception
     */
    private function __construct($serviceName, $config)
    {
        // 实例化客户端
        $this->client = new Client(SWOOLE_SOCK_TCP);
        // 设置参数
        $this->client->set([
            'open_length_check' => true,        // 打开包长检测特性(默认值:false)
            'package_length_type' => 'N',       //
            'package_length_offset' => 0,       // 第N个字节是包长度的值
            'package_body_offset' => 4,         // 第几个字节开始计算长度
            'package_max_length' => 33554432,   // 协议最大长度(32M)
        ]);

        if (!$this->client->connect(
            $config['server_host'],
            $config['port'],
            $config['timeout']
        )) {
            $message = $serviceName . ' connect failed. Error：' . $this->client->errCode;
            throw new Exception($message);
        }
    }

    /**
     * 获取服务器实例
     * @param string $serviceName
     * @param array $config
     * @return TcpClient
     * @throws Exception
     */
    public static function getService(string $serviceName, array $config): TcpClient
    {
        if (empty(static::$instance[$serviceName])) {
            static::$instance[$serviceName] = new static($serviceName, $config);
        }

        return static::$instance[$serviceName];
    }

    /**
     * 指定服务句柄,一般为RPC服务的类名
     * @param string $handler
     * @param array $construct
     * @return TcpClient
     */
    public function handler(string $handler, array $construct = []): TcpClient
    {
        $this->data['handler'] = $handler;
        $this->data['construct'] = $construct;
        return $this;
    }

    /**
     * 指定一个远程服务句柄的方法
     * @param string $method 方法名
     * @param mixed $args 执行的参数列表
     * @return bool
     * @throws Exception
     */
    public function __call(string $method, $args)
    {
        $time = time();
        $encode = $this->data['handler'] . $method;
        $saltKey = self::SALT_VALUE . $time . self::VERSION;
        $token = $this->authcode($encode, 'encode', $saltKey);
        $this->data['method'] = $method;
        $this->data['args'] = $args;
        $this->data['version'] = self::VERSION;
        $this->data['time'] = $time;
        $this->data['token'] = $token;

        return $this->sendAndRecv();
    }

    /**
     * 发送和接受消息
     * @return bool
     * @throws Exception
     */
    protected function sendAndRecv(): bool
    {
        try {
            $requestData = PackageParser::encode($this->data);
        } catch (Exception $exception) {
            $msg = sprintf(
                'tcp PackageParser encode error Message=%s code=%s file=%s line=%d',
                $exception->getMessage(),
                $exception->getCode(),
                $exception->getFile(),
                $exception->getLine()
            );
            throw new Exception($msg);
        }

        try {
            $this->client->send($requestData);
            $response = $this->client->recv();
        } catch (Exception $exception) {
            $msg = sprintf(
                'tcp server error Message=%s code=%s file=%s line=%d',
                $exception->getMessage(),
                $exception->getCode(),
                $exception->getFile(),
                $exception->getLine()
            );
            throw new Exception($msg);
        }

        try {
            $response = PackageParser::decode($response);
        } catch (Exception $exception) {
            $msg = sprintf(
                'tcp PackageParser decode error Message=%s code=%s file=%s line=%d',
                $exception->getMessage(),
                $exception->getCode(),
                $exception->getFile(),
                $exception->getLine()
            );
            throw new Exception($msg);
        }

        if (is_array($response) && isset($response['code']) && $response['code'] == 0) {
            return (bool) $response['response_data'];
        } elseif (is_array($response) && isset($response['code']) && $response['code'] != 500) {
            throw new BusinessException($response['msg'], (array)$response['response_data'], $response['code']);
        } else {
            throw new Exception('tcp server error response:' . var_export($response, true));
        }
    }

    /**
     * 加解密函数
     * @param $string
     * @param string $operation
     * @param string $key
     * @param int $expiry
     * @return false|string
     */
    protected function authcode($string, string $operation = 'decode', string $key = '', int $expiry = 180)
    {
        $ckey_length = 4;
        $key = md5($key);
        $keya = md5(substr($key, 0, 16));
        $keyb = md5(substr($key, 16, 16));
        $keyc = $ckey_length ? ($operation == 'decode' ? substr($string, 0, $ckey_length) : substr(md5(microtime()), -$ckey_length)) : '';

        $cryptkey = $keya . md5($keya . $keyc);
        $key_length = strlen($cryptkey);

        $string = $operation == 'decode' ? base64_decode(substr($string, $ckey_length)) : sprintf('%010d', $expiry ? $expiry + time() : 0) . substr(md5($string . $keyb), 0, 16) . $string;
        $string_length = strlen($string);

        $result = '';
        $box = range(0, 255);

        $rndkey = array();
        for ($i = 0; $i <= 255; $i++) {
            $rndkey[$i] = ord($cryptkey[$i % $key_length]);
        }

        for ($j = $i = 0; $i < 256; $i++) {
            $j = ($j + $box[$i] + $rndkey[$i]) % 256;
            $tmp = $box[$i];
            $box[$i] = $box[$j];
            $box[$j] = $tmp;
        }

        for ($a = $j = $i = 0; $i < $string_length; $i++) {
            $a = ($a + 1) % 256;
            $j = ($j + $box[$a]) % 256;
            $tmp = $box[$a];
            $box[$a] = $box[$j];
            $box[$j] = $tmp;
            $result .= chr(ord($string[$i]) ^ ($box[($box[$a] + $box[$j]) % 256]));
        }

        if ($operation == 'decode') {
            if ((substr($result, 0, 10) == 0 || substr($result, 0, 10) - time() > 0) && substr($result, 10, 16) == substr(md5(substr($result, 26) . $keyb), 0, 16)) {
                return substr($result, 26);
            } else {
                return '';
            }
        } else {
            return $keyc . str_replace('=', '', base64_encode($result));
        }
    }

    /**
     * 关闭swoole客户端连接
     * @return void
     */
    public function __destruct()
    {
        if (!is_null($this->client) && $this->client->isConnected()) {
            $this->client->close();
        }
    }
}