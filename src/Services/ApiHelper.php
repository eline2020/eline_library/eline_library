<?php

namespace Eline\Services;

use Eline\Tool\Json;
use GuzzleHttp\Client;
use function GuzzleHttp\Promise\settle;

class ApiHelper
{
    /**
     * 请求并记录日志
     * @param array $params
     * @return array
     * @throws mixed
     */
    public static function guzHttpRequestWithLog(array $params): array
    {
        $nowMs = microtime(true);
        try {
            $response = self::multiGuzHttpRequest($params);
            trace([
                'req_args' => $params,
                'resp_body' => $response ?? [],
                'elapsed' => bcsub(microtime(true), $nowMs, 6)
            ]);
        } catch (\Exception $exception) {
            trace([
                'req_args' => $params,
                'resp_body' => getFullException($exception),
                'elapsed' => bcsub(microtime(true), $nowMs, 6)
            ]);
        }
        return $response ?? [];
    }

    /**
     * @param array $params
     * @return array
     * @throws mixed
     */
    public static function multiGuzHttpRequest(array $params): array
    {
        $client = new Client();
        // 处理数据
        $promises = [];
        foreach ($params as $item) {
            $param = [
                'timeout' => $item['timeout'] ?? 30
            ];
            if (!empty($item['json'])) {
                $param['json'] = $item['json'] ?? [];
            } elseif (!empty($item['query'])) {
                $param['query'] = $item['query'] ?? [];
            } elseif (!empty($item['form_params'])) {
                $param['form_params'] = $item['form_params'] ?? [];
            } elseif (!empty($item['body'])) {
                if (is_array($item['body']) || is_object($item['body'])) {
                    $param['body'] = Json::encode($item['body']);
                } elseif (is_string($item['body'])) {
                    $param['body'] = $item['body'];
                }
            }
            empty($item['headers']) && $param['headers'] = $item['headers'];

            // 判断请求方式,处理promise
            $sourceKey = empty($item['source_no']) ? $item['source_from'] : $item['source_from'] . '_' . $item['source_no'];
            if (empty($item['method']) || strtolower($item['method']) == 'get') {
                $promises[$sourceKey] = $client->getAsync($item['url'], $param);
            } else {
                $promises[$sourceKey] = $client->postAsync($item['url'], $param);
            }
        }
        // 处理响应结果
        $response = [];
        $results = settle($promises)->wait();
        foreach ($results as $sourceKey => $res) {
            if ($res['state'] == 'fulfilled') {
                $responseData = $res['value']->getBody()->getContents();
                $response[$sourceKey] = Json::decode($responseData);
            } elseif ($res['state'] == 'rejected') {
                trace([
                    'key' => $sourceKey,
                    'req_args' => $params,
                    'msg' => $res['reason']->getMessage()
                ]);
                $response[$sourceKey] = [];
            }
        }
        return $response;
    }
}
