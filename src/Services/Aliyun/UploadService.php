<?php

namespace Eline\Services\Aliyun;

use Exception;
use OSS\Core\OssException;
use OSS\OssClient;

class UploadService
{
    /**
     * 上传文件类型
     * @var array
     */
    const UPLOAD_TYPE = ['putObject', 'uploadFile'];

    /**
     * UploadService对象实例
     * @var UploadService|null
     */
    protected static ?UploadService $instance = null;

    /**
     * 配置信息
     * @var array
     */
    protected static array $config = [];

    public function __construct(array $config)
    {
        self::$config = $config;
    }

    /**
     * 单例模式
     * @param array $config
     * @return UploadService|null
     */
    public static function getInstance(array $config): ?UploadService
    {
        if (self::$instance == null) {
            self::$instance = new self($config);
        }
        return self::$instance;
    }

    /**
     * 阿里云上传文件
     * @param string $filePath
     * @param string $uploadPath
     * @param string $uploadType
     * @return string
     * @throws mixed
     */
    public function uploadFileData(string $filePath, string $uploadPath = '', string $uploadType = 'putObject'): string
    {
        if (!in_array($uploadType, self::UPLOAD_TYPE)) {
            throw new Exception('上传文件类型不正确');
        }
        $pathInfo = pathinfo($filePath);
        empty($uploadPath) && $uploadPath = self::$config['file_path'];
        $object = $uploadPath . $pathInfo['basename'];
        try {
            $ossClient = new OssClient(self::$config['access_key_id'], self::$config['access_key_secret'], self::$config['endpoint']);
            // 设置超时时间为30秒
            $ossClient->setTimeout(30);
            $this->$uploadType($ossClient, $filePath, self::$config['bucket'], $object);
        } catch (OssException $exception) {
            // TODO 记录日志
            throw new Exception($exception->getMessage());
        }
        return $pathInfo['basename'];
    }

    /**
     * 获取文件内容
     * @param string $fileName
     * @param string $uploadPath
     * @return string
     * @throws mixed
     */
    public function getFileData(string $fileName, string $uploadPath = ''): string
    {
        empty($uploadPath) && $uploadPath = self::$config['file_path'];
        $object = $uploadPath . $fileName;
        try{
            $ossClient = new OssClient(self::$config['access_key_id'], self::$config['access_key_secret'], self::$config['endpoint']);
            $content = $ossClient->getObject(self::$config['bucket'], $object);
        } catch(OssException $exception) {
            // TODO 记录日志
            throw new Exception($exception->getMessage());
        }
        return $content;
    }

    /**
     * 下载文件
     * @param string $fileName
     * @param string $uploadPath
     * @return bool
     */
    public function downloadFile(string $fileName, string $uploadPath = ''): bool
    {
        empty($uploadPath) && $uploadPath = self::$config['file_path'];
        $object =  rtrim($uploadPath, '/') . '/' . $fileName;
        $localFile = __DIR__ . '/' . $fileName;
        $options = array(
            OssClient::OSS_FILE_DOWNLOAD => $localFile
        );
        try{
            $ossClient = new OssClient(self::$config['access_key_id'], self::$config['access_key_secret'], self::$config['endpoint']);
            $ossClient->getObject(self::$config['bucket'], $object, $options);
        } catch(OssException $exception) {
            // TODO 记录日志
            return false;
        }
        return true;
    }

    /**
     * 上传文件
     * @param OssClient $ossClient
     * @param string $filePath
     * @param string $bucket
     * @param string $object
     * @return void
     * @throws OssException
     */
    public function uploadFile(OssClient $ossClient, string $filePath, string $bucket, string $object)
    {
        // 获取文件大小
        $fileSize = filesize($filePath);
        // 执行上传，并设置Content-Length头部
        $options = array(
            OssClient::OSS_HEADERS => array(
                'Content-Length' => $fileSize
            )
        );
        // 设置超时时间为30秒
        $ossClient->setTimeout(30);
        // 存储空间名称、对象、文件路径
        $ossClient->uploadFile($bucket, $object, $filePath, $options);
    }

    /**
     * 上传文件
     * @param OssClient $ossClient
     * @param string $filePath
     * @param string $bucket
     * @param string $object
     * @return void
     */
    public function putObject(OssClient $ossClient, string $filePath, string $bucket, string $object)
    {
        $fileContent = file_get_contents($filePath);
        // 执行上传，并设置Content-Length头部
        $options = array(
            OssClient::OSS_HEADERS => array(
                'Content-Length' => strlen($fileContent)
            )
        );
        // 存储空间名称、对象、文件路径
        $ossClient->putObject($bucket, $object, $fileContent, $options);
    }
}